import React from 'react';
import styled from 'styled-components';
import { logo } from '../../assets';

const Signin = (props) => {
  return (
    <MainDiv>
      <LogoDiv>
        <LogoContentDiv>
          <Img src={logo} />
        </LogoContentDiv>
      </LogoDiv>
      <DataDiv>{props.children}</DataDiv>
    </MainDiv>
  );
};

const Img = styled.img`
  width: 100%;
`;

const MainDiv = styled.div`
  width: 100%;
  height: 100vh;
  background-color: #0288d1;
  overflow: hidden;
`;

const LogoDiv = styled.div`
  float: left;
  position: relative;
  width: 40%;
  height: 100%;
`;

const DataDiv = styled.div`
  float: right;
  position: relative;
  width: 60%;
  height: 100%;
  background-color: white;
  border-radius: 40px 0px 0px 40px;
`;

const LogoContentDiv = styled.div`
  margin: auto;
  padding: 40% 0;
`;

export default Signin;
