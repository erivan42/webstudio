import React, { useState, useEffect } from 'react';
import { Grid, TextField, Link } from '@material-ui/core/';
import Structure from './Structure';
import { Button, ButtonWhite } from '../../components';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { postUser, checkUser } from '../../actions/loginAction';
import { errorMessages } from '../../utils/constants';

const Signup = (props) => {
  const [login, setLogin] = useState({ email: '', password: '' });
  const [error, setError] = useState(null);
  const history = useHistory();

  useEffect(() => {
    props
      .checkUser()
      .then((resp) => {
        if (resp.logged) {
          history.push({ pathname: '/dashboard' });
        }
      })
      .catch((err) => console.log(err));
  }, []);

  const onClick = () => {
    props.postUser(login).then((res) => {
      if (res.user) {
        history.push({ pathname: '/dashboard' });
      } else {
        setError(res.info.message);
      }
    });
  };

  const onChange = (ev) => {
    const newlogin = { ...login };
    newlogin[ev.target.name] = ev.target.value;
    setLogin(newlogin);
  };

  const errorTxt = <ErrorTxt>*{errorMessages[error]}</ErrorTxt>;

  return (
    <Structure>
      <a href="/signin">
        <ButtonSignIn>
          <ButtonWhite text={'SIGNUP'} />
        </ButtonSignIn>
      </a>

      <ContentDiv>
        <Paper>
          <Title>LOGIN</Title>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email"
            name="email"
            onChange={onChange}
            autoComplete="email"
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Senha"
            type="password"
            id="password"
            onChange={onChange}
            autoComplete="current-password"
          />

          <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">
                {''}
              </Link>
            </Grid>
            <Grid item>
              <Link href="#" variant="body2">
                {'Esqueceu a senha?'}
              </Link>
            </Grid>
          </Grid>

          {error ? errorTxt : null}

          <Button
            text="LOGIN"
            onClick={(ev) => onClick()}
            fullWidth
          />
        </Paper>
      </ContentDiv>
    </Structure>
  );
};

const Title = styled.h1`
  margin: 0 auto;
  width: 30%;
  color: #707070;
`;

const ContentDiv = styled.div`
  width: 50%;
  height: 50%;
  margin: auto;
  padding: 25% 0;
`;

const Paper = styled.div`
  display: 'flex';
  flexdirection: 'column';
  alignitems: 'center';
`;

const ButtonSignIn = styled.div`
  position: relative;
  float: right;
  right: 5%;
  margin: 1em;
`;

const ErrorTxt = styled.p`
  color: red;
`;

const mapDispatchToProps = { postUser, checkUser };

export default connect(null, mapDispatchToProps)(Signup);
