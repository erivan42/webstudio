import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { logoutUser } from '../../actions/loginAction';

const Logout = (props) => {
  const history = useHistory();

  useEffect(() => {
    props
      .logoutUser(props.user)
      .then((resp) => {
        history.push({ pathname: '/signup' });
      })
      .catch((err) => console.log(err));
  }),
    [];

  return null;
};

const mapStateToProps = (state) => ({
  user: state.loginReducer,
});

const mapDispatchToProps = { logoutUser };

export default connect(mapStateToProps, mapDispatchToProps)(Logout);
