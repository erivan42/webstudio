import React from 'react';
import { Grid, TextField, Link } from '@material-ui/core/';
import Structure from './Structure';
import { Button, ButtonWhite } from '../../components';
import styled from 'styled-components';

const Signin = () => {
  const onClick = () => {
    console.log('here');
  };

  return (
    <Structure>
      <a href="/signup">
        <ButtonSignIn>
          <ButtonWhite text={'LOGIN'} />
        </ButtonSignIn>
      </a>

      <ContentDiv>
        <Paper>
          <Title>SIGN UP</Title>
          <Grid container spacing={3} container>
            <Grid item xs={6}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="nome"
                label="Nome"
                name="nome"
                autoComplete="nome"
                autoFocus
              />
            </Grid>
            <Grid item xs={6}>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="sobrenome"
                label="Sobrenome"
                name="sobrenome"
                autoComplete="sobrenome"
              />
            </Grid>
          </Grid>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email"
            name="email"
            autoComplete="email"
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Senha"
            type="password"
            id="password"
            autoComplete="current-password"
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password2"
            label="Confirmar Senha"
            type="password"
            id="password2"
            autoComplete="current-password"
          />

          <Button text="Sign Up" onClick={onClick} fullWidth />

          <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link href="#" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </Paper>
      </ContentDiv>
    </Structure>
  );
};

const Title = styled.h1`
  margin: 0 auto;
  width: 30%;
  color: #707070;
`;

const ContentDiv = styled.div`
  width: 50%;
  height: 50%;
  margin: auto;
  padding: 15% 0;
`;

const Paper = styled.div`
  display: 'flex';
  flexdirection: 'column';
  alignitems: 'center';
`;

const ButtonSignIn = styled.div`
  position: relative;
  float: right;
  right: 5%;
  margin: 1em;
`;

export default Signin;
