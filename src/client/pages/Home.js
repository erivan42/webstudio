import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import { checkUser } from '../actions/loginAction';

const Home = (props) => {
  const history = useHistory();

  useEffect(() => {
    props
      .checkUser()
      .then((resp) => {
        if (resp.logged) {
          history.push({ pathname: '/dashboard' });
        } else {
          history.push({ pathname: '/signup' });
        }
      })
      .catch((err) => console.log(err));
  });

  return null;
};

const mapDispatchToProps = { checkUser };

export default connect(null, mapDispatchToProps)(Home);
