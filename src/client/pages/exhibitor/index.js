import React, { useState } from 'react';
import { useLocation } from 'react-router-dom';
import {
  InputTxt,
  RadioGroupChoose,
  ActivityView,
  ComboBox,
  Ranger,
  Characters,
  Asks,
  Checkbox,
} from '../../components';
import {
  frametype,
  themesByFrame,
  leftRightBothOptions,
  exhibitorStructures,
  elementsImage,
  unitsOptions,
  errorMessages,
} from '../../utils/constants';

const Exhibitor = () => {
  const location = useLocation();
  const [activity, setActivity] = useState(location.state.question);

  const handleConfig = (value, field) => {
    const newObj = { ...activity };
    newObj.config[field] = value;

    if (field === 'frameNumber') {
      newObj.config.frameNumber = parseInt(value, 10);
    }

    if (field === 'structure') {
      activity.params.objectList =
        activity.config.structure === 'sum'
          ? [unitsOptions[0].value]
          : [elementsImage[0].value, elementsImage[1].value];
    }

    setActivity(newObj);
  };

  const handleParams = (value, field) => {
    const newObj = { ...activity };
    newObj.params[field] = value;

    if (field === 'displayPosition') {
      newObj.params.displayPosition = JSON.parse(
        activity.params.displayPosition,
      );
    }

    setActivity(newObj);
  };

  const quantityRange = (
    <Ranger
      txt={'Elementos em cada quadrado'}
      value={activity.params.quantityRange}
      onChange={handleParams}
      field={'quantityRange'}
      min={2}
      max={4}
    />
  );

  return (
    <ActivityView activity={activity} title={'Atividade'}>
      <InputTxt
        label={'Titulo da Atividade'}
        defaultValue={activity.config.name}
        onChange={handleConfig}
        field={'name'}
        validators={['required']}
        errorMessages={[errorMessages.required]}
      />

      <RadioGroupChoose
        labelradio={'Cenários'}
        options={frametype}
        choose={activity.config.frameStyle}
        onChange={handleConfig}
        field={'frameStyle'}
      />

      <RadioGroupChoose
        labelradio={'Temas do Cenário'}
        options={themesByFrame[activity.config.frameStyle].themes}
        choose={activity.config.frameNumber.toString()}
        onChange={handleConfig}
        field={'frameNumber'}
      />

      <ComboBox
        txt={'Estrutura do Expositor'}
        options={exhibitorStructures}
        value={activity.config.structure}
        onChange={handleConfig}
        field={'structure'}
      />

      <Ranger
        txt={'Quantidade de Questões'}
        value={activity.params.quantity}
        onChange={handleParams}
        field={'quantity'}
        max={20}
      />

      <Ranger
        txt={'Quantidade de Quadrados'}
        value={activity.params.squaresQuantity}
        onChange={handleParams}
        field={'squaresQuantity'}
        min={3}
        max={6}
      />

      {activity.config.structure === 'sum' ? quantityRange : null}

      <ComboBox
        txt={'Posição do Expositor'}
        options={leftRightBothOptions}
        value={JSON.stringify(activity.params.displayPosition)}
        onChange={handleParams}
        field={'displayPosition'}
      />

      <Checkbox
        txt={'Elementos a serem utiliziados'}
        values={activity.params.objectList}
        options={
          activity.config.structure === 'sum'
            ? unitsOptions
            : elementsImage
        }
        onChange={handleParams}
        field={'objectList'}
        width={500}
        min={2}
      />

      <Characters
        txt={'Lista de Personagens'}
        array={activity.params.charList}
        onChange={handleParams}
        field={'charList'}
      />

      <Asks
        txt={'Perguntas das Questões'}
        array={activity.params.ask}
        onChange={handleParams}
        field={'ask'}
      />
    </ActivityView>
  );
};

export default Exhibitor;
