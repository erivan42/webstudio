import React, { useState } from 'react';
import { useLocation } from 'react-router-dom';
import {
  InputTxt,
  RadioGroupChoose,
  ActivityView,
  Ranger,
  Asks,
} from '../../components';
import {
  frametype,
  themesByFrame,
  orderOptions,
  errorMessages,
} from '../../utils/constants';

const Selections = () => {
  const location = useLocation();
  const [activity, setActivity] = useState(location.state.question);

  const handleConfig = (value, field) => {
    const newObj = { ...activity };
    newObj.config[field] = value;

    if (field === 'frameNumber') {
      newObj.config.frameNumber = parseInt(value, 10);
    }

    setActivity(newObj);
  };

  const handleParams = (value, field) => {
    const newObj = { ...activity };
    newObj.params[field] = value;

    if (field === 'squaresQuantity') {
      newObj.params.squaresQuantity = value > 2 ? value : 2;
    }

    if (field === 'range') {
      if (value[1] - value[0] < 2) {
        return;
      }
    }

    setActivity(newObj);
  };

  return (
    <ActivityView activity={activity} title={'Atividade'}>
      <InputTxt
        label={'Titulo da Atividade'}
        defaultValue={activity.config.name}
        onChange={handleConfig}
        field={'name'}
        validators={['required']}
        errorMessages={[errorMessages.required]}
      />

      <RadioGroupChoose
        labelradio={'Cenários'}
        options={frametype}
        choose={activity.config.frameStyle}
        onChange={handleConfig}
        field={'frameStyle'}
      />

      <RadioGroupChoose
        labelradio={'Temas do Cenário'}
        options={themesByFrame[activity.config.frameStyle].themes}
        choose={activity.config.frameNumber.toString()}
        onChange={handleConfig}
        field={'frameNumber'}
      />

      <RadioGroupChoose
        labelradio={'Ordem'}
        options={orderOptions}
        choose={activity.params.order}
        onChange={handleParams}
        field={'order'}
      />

      <Ranger
        txt={'Quantidade de Quadrados'}
        value={activity.params.squaresQuantity}
        onChange={handleParams}
        field={'squaresQuantity'}
        min={3}
        max={6}
      />

      <Ranger
        txt={'Faixa de Números'}
        value={activity.params.range}
        onChange={handleParams}
        field={'range'}
        max={12}
      />

      <Ranger
        txt={'Quantidade de Questões'}
        value={activity.params.quantity}
        onChange={handleParams}
        field={'quantity'}
        max={20}
      />

      <Asks
        txt={'Perguntas das Questões'}
        array={activity.params.ask}
        onChange={handleParams}
        field={'ask'}
      />
    </ActivityView>
  );
};

export default Selections;
