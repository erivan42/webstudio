import React, { useState } from 'react';
import { useLocation } from 'react-router-dom';
import {
  InputTxt,
  RadioGroupChoose,
  ComboBox,
  ActivityView,
  Ranger,
  Asks,
} from '../../components';
import { moreLessWordList } from '../../utils/';
import {
  totemStructures,
  themesByFrame,
  frametype,
  errorMessages,
} from '../../utils/constants';

const Totem = () => {
  const location = useLocation();
  const [activity, setActivity] = useState({
    ...location.state.question,
  });

  const handleConfig = (value, field) => {
    const newObj = { ...activity };
    newObj.config[field] = value;

    if (field === 'frameNumber') {
      newObj.config.frameNumber = parseInt(value, 10);
    }

    setActivity(newObj);
  };

  const handleParams = (value, field) => {
    const newObj = { ...activity };

    newObj.params[field] = value;

    setActivity(newObj);
  };

  return (
    <ActivityView activity={activity} title={'Atividade'}>
      <InputTxt
        label={'Titulo da Atividade'}
        defaultValue={activity.config.name}
        onChange={handleConfig}
        field={'name'}
        validators={['required']}
        errorMessages={[errorMessages.required]}
      />

      <RadioGroupChoose
        labelradio={'Cenários'}
        options={frametype}
        choose={activity.config.frameStyle}
        onChange={handleConfig}
        field={'frameStyle'}
      />

      <RadioGroupChoose
        labelradio={'Temas do Cenário'}
        options={themesByFrame[activity.config.frameStyle].themes}
        choose={activity.config.frameNumber.toString()}
        onChange={handleConfig}
        field={'frameNumber'}
      />

      <ComboBox
        txt={'Estrutura do Totem'}
        options={totemStructures}
        value={activity.config.structure}
        onChange={handleConfig}
        field={'structure'}
      />

      <Ranger
        txt={'Quantidade de Questões'}
        value={activity.params.quantity}
        onChange={handleParams}
        field={'quantity'}
        max={20}
      />

      <Ranger
        txt={'Faixa de Números'}
        value={activity.params.range}
        onChange={handleParams}
        field={'range'}
        max={12}
      />

      <Asks
        txt={'Perguntas das Questões'}
        array={activity.params.ask}
        onChange={handleParams}
        field={'ask'}
        validator={
          activity.config.structure !== 'oneColumn'
            ? moreLessWordList
            : null
        }
      />
    </ActivityView>
  );
};

export default Totem;
