import React, { useState } from 'react';
import { useLocation } from 'react-router-dom';
import {
  InputTxt,
  RadioGroupChoose,
  ComboBox,
  ActivityView,
  Ranger,
  Characters,
  Asks,
} from '../../components';
import {
  positionsStructures,
  themesByFrame,
  frametype,
  leftRightOptions,
  leftRightBothOptions,
  errorMessages,
} from '../../utils/constants';
import { structures } from './structures';

const Positions = () => {
  const location = useLocation();
  const [activity, setActivity] = useState({
    ...location.state.question,
  });

  const handleConfig = (value, field) => {
    const newObj = { ...activity };
    newObj.config[field] = value;

    if (field === 'frameNumber') {
      newObj.config.frameNumber = parseInt(value, 10);
    }

    if (field === 'structure') {
      newObj.params.ask = structures[activity.config.structure].asks;

      if (value === 'aboveAndBelow') {
        delete newObj.params.numCharsToShow;
      } else {
        if (value === 'range' || value === 'leftRight') {
          newObj.params.numCharsToShow = 2;
        } else {
          newObj.params.numCharsToShow = [1, 2];
        }
      }

      if (value !== 'order') {
        delete newObj.params.first;
      } else {
        newObj.params.first = 'left';
      }

      if (value !== 'insideOutside') {
        delete newObj.params.jeepSide;
      } else {
        newObj.params.jeepSide = `["left"]`;
      }
    }

    setActivity(newObj);
  };

  const handleParams = (value, field) => {
    const newObj = { ...activity };
    newObj.params[field] = value;

    if (field === 'jeepSide') {
      newObj.params.jeepSide = JSON.parse(activity.params.jeepSide);
    }

    setActivity(newObj);
  };

  const numCharsToShow = (
    <Ranger
      txt={'Quantidade de Personagens a exibir'}
      value={activity.params.numCharsToShow}
      onChange={handleParams}
      field={'numCharsToShow'}
      max={7}
    />
  );

  const first = (
    <RadioGroupChoose
      labelradio={'Primeira Posição'}
      options={leftRightOptions}
      choose={activity.params.first}
      onChange={handleParams}
      field={'first'}
    />
  );

  const jeepSide = (
    <ComboBox
      txt={'Posição do Jeep'}
      options={leftRightBothOptions}
      value={JSON.stringify(activity.params.jeepSide)}
      onChange={handleParams}
      field={'jeepSide'}
    />
  );

  return (
    <ActivityView activity={activity} title={'Atividade'}>
      <InputTxt
        label={'Titulo da Atividade'}
        defaultValue={activity.config.name}
        onChange={handleConfig}
        field={'name'}
        validators={['required']}
        errorMessages={[errorMessages.required]}
      />

      <RadioGroupChoose
        labelradio={'Cenários'}
        options={frametype}
        choose={activity.config.frameStyle}
        onChange={handleConfig}
        field={'frameStyle'}
      />

      <RadioGroupChoose
        labelradio={'Temas do Cenário'}
        options={themesByFrame[activity.config.frameStyle].themes}
        choose={activity.config.frameNumber.toString()}
        onChange={handleConfig}
        field={'frameNumber'}
      />

      <ComboBox
        txt={'Estrutura de Posições'}
        options={positionsStructures}
        value={activity.config.structure}
        onChange={handleConfig}
        field={'structure'}
      />

      <Ranger
        txt={'Quantidade de Questões'}
        value={activity.params.quantity}
        onChange={handleParams}
        field={'quantity'}
        max={20}
      />

      {activity.params.numCharsToShow ? numCharsToShow : null}

      {activity.config.structure === 'order' ? first : null}

      {activity.config.structure === 'insideOutside'
        ? jeepSide
        : null}

      <Characters
        txt={'Lista de Personagens'}
        array={activity.params.charList}
        onChange={handleParams}
        field={'charList'}
      />

      <Asks
        txt={'Perguntas das Questões'}
        array={activity.params.ask}
        onChange={handleParams}
        field={'ask'}
        validator={structures[activity.config.structure].validator}
      />
    </ActivityView>
  );
};

export default Positions;
