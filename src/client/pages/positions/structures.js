import {
  aboveBelowWords,
  insideOutiseWords,
  positionWords,
  leftRightWords,
  rangeWords,
} from '../../utils/constants';
import { checkWordList } from '../../utils/';

const structures = {
  range: {
    validator: (text) => checkWordList(text, rangeWords),
    asks: [
      'Quem está mais perto do jipe?',
      'Quem está mais longe do jipe?',
      'Quem está mais distante do gênio?',
    ],
  },
  insideOutside: {
    validator: (text) => checkWordList(text, insideOutiseWords),
    asks: ['Quem está dentro do jipe?', 'Quem está fora do jipe?'],
  },
  leftRight: {
    validator: (text) => checkWordList(text, leftRightWords),
    asks: [
      'Quem está à direita da estátua?',
      'Quem está à esquerda da estátua?',
    ],
  },
  aboveAndBelow: {
    validator: (text) => checkWordList(text, aboveBelowWords),
    asks: [
      'Quem está em cima da estátua?',
      'Quem está acima?',
      'Quem está embaixo?',
    ],
  },
  order: {
    validator: (text) => checkWordList(text, positionWords),
    asks: [
      'Quem é o primeiro da fila?',
      'Quem é o sétimo?',
      'Quem é o último?',
    ],
  },
};

export { structures };
