import React, { useState } from 'react';
import { useLocation } from 'react-router-dom';
import {
  InputTxt,
  RadioGroupChoose,
  ActivityView,
  Ranger,
  Asks,
  Characters,
} from '../../components';
import {
  frametype,
  themesByFrame,
  operatorOptions,
  errorMessages,
} from '../../utils/constants';

const Equality = () => {
  const location = useLocation();
  const [activity, setActivity] = useState(location.state.question);

  const handleConfig = (value, field) => {
    const newObj = { ...activity };
    newObj.config[field] = value;

    if (field === 'frameNumber') {
      newObj.config.frameNumber = parseInt(value, 10);
    }

    setActivity(newObj);
  };

  const handleParams = (value, field) => {
    const newObj = { ...activity };

    newObj.params[field] = value;

    setActivity(newObj);
  };

  return (
    <ActivityView activity={activity} title={'Atividade'}>
      <InputTxt
        label={'Titulo da Atividade'}
        defaultValue={activity.config.name}
        onChange={handleConfig}
        field={'name'}
        validators={['required']}
        errorMessages={[errorMessages.required]}
      />

      <RadioGroupChoose
        labelradio={'Cenários'}
        options={frametype}
        choose={activity.config.frameStyle}
        onChange={handleConfig}
        field={'frameStyle'}
      />

      <RadioGroupChoose
        labelradio={'Temas do Cenário'}
        options={themesByFrame[activity.config.frameStyle].themes}
        choose={activity.config.frameNumber.toString()}
        onChange={handleConfig}
        field={'frameNumber'}
      />

      <Ranger
        txt={'Quantidade de Questões'}
        value={activity.params.quantity}
        onChange={handleParams}
        field={'quantity'}
        max={20}
      />

      <Ranger
        txt={'Quantidade de Subquestões'}
        value={activity.params.subquestionsQuantity}
        onChange={handleParams}
        field={'subquestionsQuantity'}
        max={10}
      />

      <Ranger
        txt={'Faixa de Números'}
        value={activity.params.numbersRange}
        onChange={handleParams}
        field={'numbersRange'}
        max={12}
      />

      <RadioGroupChoose
        labelradio={'Operador'}
        options={operatorOptions}
        choose={activity.params.operator}
        onChange={handleParams}
        field={'operator'}
      />

      <Characters
        txt={'Lista de Personagens'}
        array={activity.params.charList}
        onChange={handleParams}
        field={'charList'}
        min={2}
      />

      <Asks
        txt={'Perguntas das Questões'}
        array={activity.params.ask}
        onChange={handleParams}
        field={'ask'}
      />
    </ActivityView>
  );
};

export default Equality;
