import React, { useState } from 'react';
import { useLocation } from 'react-router-dom';
import {
  InputTxt,
  RadioGroupChoose,
  ActivityView,
  ComboBox,
  Ranger,
} from '../../components';
import {
  frametype,
  directionsOptions,
  skiesType,
  errorMessages,
} from '../../utils/constants';

const Directions = () => {
  const location = useLocation();
  const [activity, setActivity] = useState(location.state.question);

  const handleConfig = (value, field) => {
    const newObj = { ...activity };
    newObj.config[field] = value;

    if (field === 'frameNumber') {
      newObj.config.frameNumber = parseInt(value, 10);
    }

    setActivity(newObj);
  };

  const handleParams = (value, field) => {
    const newObj = { ...activity };

    newObj.params[field] = value;

    if (field === 'directions') {
      newObj.params.directions = JSON.parse(value);
    }

    setActivity(newObj);
  };

  return (
    <ActivityView activity={activity} title={'Atividade'}>
      <InputTxt
        label={'Titulo da Atividade'}
        defaultValue={activity.config.name}
        onChange={handleConfig}
        field={'name'}
        validators={['required']}
        errorMessages={[errorMessages.required]}
      />

      <RadioGroupChoose
        labelradio={'Cenários'}
        options={frametype}
        choose={activity.config.frameStyle}
        onChange={handleConfig}
        field={'frameStyle'}
      />

      <RadioGroupChoose
        labelradio={'Temas do Cenário'}
        options={skiesType}
        choose={activity.config.frameNumber.toString()}
        onChange={handleConfig}
        field={'frameNumber'}
      />

      <ComboBox
        txt={'Direções'}
        options={directionsOptions}
        value={JSON.stringify(activity.params.directions)}
        onChange={handleParams}
        field={'directions'}
      />

      <InputTxt
        label={'Pergunta Unica'}
        onChange={handleParams}
        width={500}
        fullWidth
        defaultValue={activity.params.ask}
        field={'ask'}
        validators={['required']}
        errorMessages={[errorMessages.required]}
      />

      <Ranger
        txt={'Quantidade de Questões'}
        value={activity.params.quantity}
        onChange={handleParams}
        field={'quantity'}
        max={20}
      />
    </ActivityView>
  );
};

export default Directions;
