import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { AddActBtn, TableVersions } from '../../components';
import { generalConstants } from '../../utils/constants';
import { getActivities } from '../../actions/activitiesAction';

const Versions = (props) => {
  const { activities } = props.activities;

  useEffect(() => {
    props.getActivities();
  });

  return (
    <Div>
      <TableVersions activities={activities} />
      <AddActBtn />
    </Div>
  );
};

const Div = styled.div`
  margin: ${generalConstants.marginInnerCard};
`;

const mapStateToProps = (state) => ({
  activities: state.activitiesReducer,
});

const mapDispatchToProps = { getActivities };

export default connect(mapStateToProps, mapDispatchToProps)(Versions);
