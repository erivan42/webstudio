import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { AddActBtn, TableActivities } from '../../components';
import { generalConstants } from '../../utils/constants';
import { getActivities } from '../../actions/activitiesAction';

const Dashboard = (props) => {
  const { activities } = props.activities;

  useEffect(() => {
    props.getActivities();
  });

  return (
    <Div>
      <TableActivities activities={activities} />
      <AddActBtn />
    </Div>
  );
};

const Div = styled.div`
  margin: ${generalConstants.marginInnerCard};
`;

const mapStateToProps = (state) => ({
  activities: state.activitiesReducer,
});

const mapDispatchToProps = { getActivities };

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Dashboard);
