import React, { useState } from 'react';
import { useLocation } from 'react-router-dom';
import {
  InputTxt,
  RadioGroupChoose,
  ActivityView,
  Ranger,
  Asks,
  Characters,
} from '../../components';
import {
  themesByFrame,
  frametype,
  countingWords,
  errorMessages,
} from '../../utils/constants';
import { checkWordList } from '../../utils/';

const Counting = () => {
  const location = useLocation();
  const [activity, setActivity] = useState({
    ...location.state.question,
  });

  const handleConfig = (value, field) => {
    const newObj = { ...activity };
    newObj.config[field] = value;

    if (field === 'frameNumber') {
      newObj.config.frameNumber = parseInt(value, 10);
    }

    if (field === 'structure') {
      newObj.params.ask = [];
    }

    setActivity(newObj);
  };

  const handleParams = (value, field) => {
    const newObj = { ...activity };

    newObj.params[field] = value;

    setActivity(newObj);
  };

  return (
    <ActivityView activity={activity} title={'Atividade'}>
      <InputTxt
        label={'Titulo da Atividade'}
        defaultValue={activity.config.name}
        onChange={handleConfig}
        field={'name'}
        validators={['required']}
        errorMessages={[errorMessages.required]}
      />

      <RadioGroupChoose
        labelradio={'Cenários'}
        options={frametype}
        choose={activity.config.frameStyle}
        onChange={handleConfig}
        field={'frameStyle'}
      />

      <RadioGroupChoose
        labelradio={'Temas do Cenário'}
        options={themesByFrame[activity.config.frameStyle].themes}
        choose={activity.config.frameNumber.toString()}
        onChange={handleConfig}
        field={'frameNumber'}
      />

      <Ranger
        txt={'Quantidade de Questões'}
        value={activity.params.quantity}
        onChange={handleParams}
        field={'quantity'}
        max={20}
      />

      <Ranger
        txt={'Faixa de Números'}
        value={activity.params.listRange}
        onChange={handleParams}
        field={'listRange'}
        max={12}
      />

      <Characters
        txt={'Lista de Personagens'}
        array={activity.params.charList}
        onChange={handleParams}
        field={'charList'}
      />

      <Asks
        txt={'Perguntas das Questões'}
        array={activity.params.ask}
        onChange={handleParams}
        field={'ask'}
        validator={(txt) => checkWordList(txt, countingWords)}
      />
    </ActivityView>
  );
};

export default Counting;
