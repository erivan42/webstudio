import React, { useState, useEffect } from 'react';
import {
  InputTxt,
  ComboBox,
  RadioGroupChoose,
} from '../../components';
import QuizAlternatives from './QuizAlternatives';
import { confimation, personBlocks } from '../../utils/constants';

const Quiz = (props) => {
  const { question, onChangeQuestions, index } = props;

  const [activity, setActivity] = useState(question);

  const [alternatives, setAlternatives] = useState(
    question.showAlternative ? question.showAlternative : true,
  );

  useEffect(() => {
    onChangeQuestions(activity, index);
  }, [activity]);

  const handleAnswerValue = (item) => {
    const newObj = { ...activity };
    newObj.answer.value = item.value;

    setActivity(newObj);
  };

  const handleStatementValue = (value) => {
    const newObj = { ...activity };
    newObj.ask = value;
    setActivity(newObj);
  };

  const handleAlternatives = (value) => {
    const newObj = { ...activity };
    newObj.showAlternative = !alternatives;
    setActivity(newObj);
    setAlternatives(!alternatives);
  };

  const onChangeAlt = (alts) => {
    const newObj = { ...activity };
    newObj.alternatives = alts;
    setActivity(newObj);
  };

  return (
    <>
      <InputTxt
        label={'Pergunta'}
        defaultValue={activity.ask}
        onChange={handleStatementValue}
        fullWidth
      />

      <ComboBox
        txt={'Resposta'}
        options={personBlocks}
        value={activity.answer.value}
        onChange={handleAnswerValue}
      />

      <RadioGroupChoose
        labelradio={'Respostas Alternativas?'}
        options={confimation}
        choose={alternatives}
        onChange={handleAlternatives}
      />

      {alternatives ? (
        <QuizAlternatives
          onChangeAlt={onChangeAlt}
          data={question.alternatives}
        />
      ) : null}
    </>
  );
};

export default Quiz;
