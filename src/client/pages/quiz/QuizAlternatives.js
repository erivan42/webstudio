import React, { useState, useEffect } from 'react';
import {
  InputTxt,
  ComboBox,
  CircularBtn,
  useStyles,
} from '../../components';
import { personBlocks, getNameChar } from '../../utils/constants';
import {
  Typography,
  List,
  ListItem,
  ListItemAvatar,
  ListItemText,
  Avatar,
  Grid,
  IconButton,
  ListItemSecondaryAction,
} from '@material-ui/core/';
import { Delete, ShortText } from '@material-ui/icons';

const QuizAlternatives = (props) => {
  const { data, onChangeAlt } = props;
  const [alts, setAlts] = useState(data);
  const [value, setValue] = useState('');
  const [person, setPerson] = useState({});
  const classes = useStyles();

  useEffect(() => {
    onChangeAlt(alts);
  }, [alts]);

  const handleValue = (value) => setValue(value);

  const handlePerson = (value) => setPerson(value);

  const handleAdd = () => {
    const same = alts.find((item) => item.answer === value);

    if (!same && alts.length < 4) {
      setAlts([...alts, { answer: value, char: person }]);
    }
    setValue('');
  };

  const handleDelete = (ind) => {
    const newArray = alts.filter((_, index) => index !== ind);
    setAlts(newArray);
  };

  return (
    <div style={{ margin: '1%', width: '100%' }}>
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
      >
        <InputTxt
          label={'Alternativa'}
          defaultValue={value}
          onChange={handleValue}
        />

        <ComboBox
          txt={'Personagem'}
          options={personBlocks}
          onChange={handlePerson}
        />

        <CircularBtn onClick={handleAdd} />
      </Grid>

      <Typography variant="h6" className={classes.title}>
        Respostas Alternativas
      </Typography>

      <List>
        {alts.map((item, index) => (
          <ListItem key={index}>
            <ListItemAvatar>
              <Avatar>
                <ShortText />
              </Avatar>
            </ListItemAvatar>
            <ListItemText
              primary={item.answer}
              secondary={getNameChar(item.char)}
            />
            <ListItemSecondaryAction>
              <IconButton
                edge="end"
                aria-label="delete"
                onClick={() => handleDelete(index)}
              >
                <Delete />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        ))}
      </List>
    </div>
  );
};

export default QuizAlternatives;
