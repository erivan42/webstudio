import React, { useState } from 'react';
import { useLocation } from 'react-router-dom';
import {
  InputTxt,
  RadioGroupChoose,
  QuestionsTabs,
  ActivityView,
} from '../../components';
import {
  questionTemplate,
  errorMessages,
} from '../../utils/constants';
import QuizComp from './Quiz';
import { frametype, themesByFrame } from '../../utils/constants';

const Quiz = () => {
  const location = useLocation();
  const [activity, setActivity] = useState(location.state.question);

  const handleFrame = (value) => {
    const newObj = { ...activity };
    newObj.config.frameStyle = value;
    setActivity(newObj);
  };

  const handleNameValue = (value) => {
    const newObj = { ...activity };
    newObj.config.name = value;
    setActivity(newObj);
  };

  const handleBG = (value) => {
    const newObj = { ...activity };
    newObj.config.frameNumber = parseInt(value, 10);
    setActivity(newObj);
  };

  const onChangeQuestions = (question, index) => {
    const newObj = { ...activity };
    newObj.questions[index] = question;
    setActivity(newObj);
  };

  const onAddQuestion = (questions) => {
    const newObj = { ...activity };
    newObj.questions = questions;
    setActivity(newObj);
  };

  return (
    <ActivityView activity={activity} title={'Atividade'}>
      <InputTxt
        label={'Titulo da Atividade'}
        defaultValue={activity.config.name}
        onChange={handleNameValue}
        validators={['required']}
        errorMessages={[errorMessages.required]}
      />

      <RadioGroupChoose
        labelradio={'Cenários'}
        options={frametype}
        choose={activity.config.frameStyle}
        onChange={handleFrame}
      />

      <RadioGroupChoose
        labelradio={'Temas do Cenário'}
        options={themesByFrame[activity.config.frameStyle].themes}
        choose={activity.config.frameNumber.toString()}
        onChange={handleBG}
      />

      <QuestionsTabs
        onAddQuestion={onAddQuestion}
        questions={activity.questions}
        structureQuestion={questionTemplate['quiz']['structure']}
      >
        <QuizComp onChangeQuestions={onChangeQuestions} />
      </QuestionsTabs>
    </ActivityView>
  );
};

export default Quiz;
