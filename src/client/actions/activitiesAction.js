import axios from 'axios';
import {
  GET_ACTIVITIES,
  ADD_ACT,
  EDIT_ACT,
  REMOVE_ACT,
} from './activitiesReduce';

export const getActivities = () => (dispatch) => {
  return axios
    .get('api/getActivities')
    .then((res) => {
      return dispatch({
        type: GET_ACTIVITIES,
        payload: res.data,
      });
    })
    .catch((error) => {
      console.log(error);
    });
};

export const generateApplication = () => (dispatch) => {
  return new Promise((resolve, reject) =>
    axios
      .get('api/generateApplication')
      .then((res) => {
        resolve(res);
      })
      .catch((error) => {
        reject(error);
      }),
  );
};

export const addActivity = (activity) => (dispatch) => {
  activity.config.timestamp = new Date();
  activity.config.author = 'CITS Devs';

  return axios
    .post('api/addActivity', { activity })
    .then((res) =>
      dispatch({
        type: ADD_ACT,
        payload: activity,
      }),
    )
    .catch((error) => {
      console.log('error', error);
    });
};

export const updateActivity = (activity, index) => (dispatch) => {
  activity.config.timestamp = new Date();
  activity.config.author = 'CITS Devs';

  return axios
    .post('api/updateActivity', { activity, index })
    .then((res) =>
      dispatch({
        type: EDIT_ACT,
        payload: activity,
      }),
    )
    .catch((error) => {
      console.log('error', error);
    });
};

export const removeActivity = (activityNumber) => (dispatch) => {
  return axios
    .post('api/removeActivity', { activityNumber })
    .then((res) =>
      dispatch({
        type: REMOVE_ACT,
        payload: activityNumber,
      }),
    )
    .catch((error) => {
      console.log('error', error);
    });
};
