import axios from 'axios';

export const checkUser = () => (dispatch) =>
  new Promise((resolve, reject) => {
    return axios
      .get('api/checkuser')
      .then((res) => {
        resolve(res.data);
      })
      .catch((error) => reject(error));
  }, 1000);

export const postUser = (user) => (dispatch) =>
  new Promise((resolve, reject) => {
    return axios
      .post('api/login', user)
      .then((res) => {
        resolve(res.data);
      })
      .catch((error) => reject(error));
  }, 1000);

export const logoutUser = (user) => (dispatch) =>
  new Promise((resolve, reject) => {
    return axios
      .post('api/logout', user)
      .then((res) => {
        resolve(res.data);
      })
      .catch((error) => reject(error));
  }, 1000);
