export const GET_ACTIVITIES = 'GET_ACTIVITIES';
export const ADD_ACT = 'ADD_ACT';
export const EDIT_ACT = 'EDIT_ACT';
export const REMOVE_ACT = 'REMOVE_ACT';

export const initialState = {
  activities: [],
  units: [],
};

const activitiesReducer = (state, action) => {
  switch (action.type) {
    case GET_ACTIVITIES:
      return {
        ...state,
        activities: action.payload.activities,
        units: action.payload.units,
      };
    case ADD_ACT:
      return {
        ...state,
        activities: [...state.activities, action.payload],
      };
    case EDIT_ACT:
      return {
        ...state,
        activities: state.activities.map((item) =>
          item.config.activityNumber ===
          action.payload.config.activityNumber
            ? action.payload
            : item,
        ),
      };
    case REMOVE_ACT:
      return {
        ...state,
        activities: state.activities.filter(
          (item) => item.config.activityNumber !== action.payload,
        ),
      };
    default:
      return initialState;
  }
};

export default activitiesReducer;
