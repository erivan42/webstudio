export const CHECK_USER = 'CHECK_USER';

export const initialState = {
  user: {},
};

const loginReducer = (state, action) => {
  switch (action.type) {
    case CHECK_USER:
      return {
        ...state,
        user: action.payload,
      };
    default:
      return initialState;
  }
};

export default loginReducer;
