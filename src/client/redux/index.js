import { applyMiddleware, createStore, combineReducers } from 'redux';
import {
  connectRouter,
  routerMiddleware,
} from 'connected-react-router';
import thunk from 'redux-thunk';

import history from './history';
import allreduces from './allreduces';

const rootReducer = combineReducers(allreduces);

const middlewares = [routerMiddleware(history), thunk];

const store = createStore(
  connectRouter(history)(rootReducer),
  applyMiddleware(...middlewares),
);

export default store;
