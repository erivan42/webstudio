import activitiesReducer from '../actions/activitiesReduce';
import loginReducer from '../actions/loginReduce';

const allreduces = { activitiesReducer, loginReducer };

export default allreduces;
