import {
  questionTemplate,
  templates,
} from './templates/questionTemplates';
import { urlApp } from '../../server/constants';

const countingWords = ['conte', 'quantos', 'quantas', 'quantidade'];
const rangeWords = ['longe', 'perto', 'distante'];
const moreWords = ['maior', 'mais'];
const lessWords = ['menor', 'menos'];
const leftRightWords = ['esquerda', 'direita'];
const positionWords = [
  'primeiro',
  'segundo',
  'terceiro',
  'quarto',
  'quinto',
  'sexto',
  'sétimo',
  'último',
];
const insideOutiseWords = ['dentro', 'fora'];
const aboveBelowWords = ['acima', 'cima', 'baixo', 'embaixo'];

const helperText = {
  mustContain: 'O texto deve conter palavras chaves',
  mustHaveOne: 'Só pode haver um conjunto de palavras chave na frase',
};

const generalConstants = {
  marginBetweenElements: '2% 1%',
  marginInnerCard: '3% 1% 1% 3%',
};

const exhibitorStructures = [
  { value: 'same', label: 'Igualdade' },
  { value: 'different', label: 'Diferença' },
  { value: 'sum', label: 'Soma' },
];

const totemStructures = [
  { value: 'oneColumn', label: 'Uma Coluna' },
  { value: 'twoColumns', label: 'Duas Colunas' },
  { value: 'threeColumns', label: 'Três Colunas' },
];

const positionsStructures = [
  { value: 'range', label: 'Perto /  Longe' },
  { value: 'insideOutside', label: 'Dentro / Fora' },
  { value: 'leftRight', label: 'Quem está a direita/esquerda' },
  { value: 'aboveAndBelow', label: 'Quem está em cima ou abaixo' },
  { value: 'order', label: 'Ordem de uma fila' },
];

const elementsImage = [
  { value: 'cherry', label: 'Cereja' },
  { value: 'grape', label: 'Uva' },
  { value: 'pear', label: 'Pêra' },
  { value: 'strawberry', label: 'Morango' },
  { value: 'banana', label: 'Banana' },
];

const personBlocks = [
  { value: 'genie', label: 'Genio' },
  { value: 'camel', label: 'Camelo' },
  { value: 'mummy', label: 'Múmia' },
  { value: 'cat', label: 'Gata' },
  { value: 'cleopatra', label: 'Cleopatra' },
  { value: 'odalisque', label: 'Odalisca' },
  { value: 'explorer', label: 'Explorador' },
];

const dotImages = [
  { label: '1', value: 'dots_1' },
  { label: '2', value: 'dots_2' },
  { label: '3', value: 'dots_3' },
  { label: '4', value: 'dots_4' },
  { label: '5', value: 'dots_5' },
  { label: '6', value: 'dots_6' },
  { label: '7', value: 'dots_7' },
  { label: '8', value: 'dots_8' },
  { label: '9', value: 'dots_9' },
  { label: '10', value: 'dots_10' },
  { label: '11', value: 'dots_11' },
  { label: '12', value: 'dots_12' },
];

const frametype = [
  {
    value: 'EGYPT',
    label: 'Egito',
  },
];

const themesByFrame = {
  EGYPT: {
    themes: [
      { value: '1', label: 'Egito - Tema 1' },
      { value: '2', label: 'Egito - Tema 2' },
      { value: '3', label: 'Egito - Tema 3' },
    ],
  },
};

const skiesType = [
  { value: '4', label: 'Céu da Manhã' },
  { value: '5', label: 'Céu da Tarde' },
  { value: '6', label: 'Céu da Noite' },
];

const orderOptions = [
  { value: 'ascend', label: 'Ascendente' },
  { value: 'descend', label: 'Descendente' },
];

const operatorOptions = [
  { value: '+', label: 'Soma' },
  { value: '-', label: 'Subtração' },
];

const directionsOptions = [
  {
    value: `["arrow_up_left","arrow_up_right","arrow_down_left","arrow_down_right"]`,
    label: 'Diagonais',
  },
  {
    value: `["arrow_up","arrow_down","arrow_left","arrow_right"]`,
    label: 'Cartesiano',
  },
  {
    value: `["arrow_up","arrow_down","arrow_left","arrow_right","arrow_up_left","arrow_up_right","arrow_down_left","arrow_down_right"]`,
    label: 'Todas as Direções',
  },
];

const confimation = [
  { value: true, label: 'Sim' },
  { value: false, label: 'Não' },
];

const displacements = [
  { value: 'row', label: 'Linha' },
  { value: 'column', label: 'Coluna' },
];

const unitsOptions = [
  { value: 'units', label: 'Unidades' },
  { value: 'tens', label: 'Dezenas' },
  { value: 'hundreds', label: 'Centenas' },
  { value: 'thousands', label: 'Milhares' },
];

const leftRightOptions = [
  { value: 'left', label: 'Esquerda' },
  { value: 'right', label: 'Direita' },
];

const leftRightBothOptions = [
  { value: `["left"]`, label: 'Esquerda' },
  { value: `["right"]`, label: 'Direita' },
  { value: `["left","right"]`, label: 'Esquerda e Direita' },
];

const aboveBelowOptions = [
  {
    value: 'above',
    label:
      'Personagem Esquerdo em cima | Personagem direito em baixo',
  },
  {
    value: 'below',
    label:
      'Personagem Esquerdo em baixo | Personagem direito em cima',
  },
];

const insideOutiseOptions = [
  { value: 'insideRight', label: 'Dentro' },
  { value: 'outsideLeft', label: 'Fora' },
];

const containerOptions = [
  { value: 'containerLeft', label: 'Container a esquerda' },
  { value: 'containerRight', label: 'Container a direita' },
];

const errorMessages = {
  email: 'Esse não é um email válido',
  required: 'Esse campo é obrigatório',
  passwordIncorrect: 'Senha incorreta',
  noEmail: 'Esse email não foi cadastrado',
  'Missing credentials': 'Preencha os campos obrigatórios',
};

const uniqueNumber = () => {
  const now = new Date();
  const timestamp = new Date().getUTCMilliseconds();
  const year = now.getFullYear().toString();
  const month =
    (now.getMonth < 9 ? '0' : '') + now.getMonth().toString(); // JS months are 0-based, so +1 and pad with 0's
  const day =
    (now.getDate < 10 ? '0' : '') + now.getDate().toString(); // pad with a 0

  return `${timestamp}${year}${month}${day}`;
};

const getNameChar = (item) => {
  const person = personBlocks.find((person) => person.value === item);

  return person ? person.label : item;
};

const getNameImageDots = (item) => {
  const img = dotImages.find((img) => img.value === item);
  return img ? img.label : item;
};

const jsonCopy = (src) => JSON.parse(JSON.stringify(src));

export {
  urlApp,
  moreWords,
  lessWords,
  aboveBelowWords,
  insideOutiseWords,
  positionWords,
  leftRightWords,
  rangeWords,
  countingWords,
  helperText,
  frametype,
  confimation,
  templates,
  questionTemplate,
  uniqueNumber,
  jsonCopy,
  personBlocks,
  getNameChar,
  displacements,
  dotImages,
  getNameImageDots,
  themesByFrame,
  generalConstants,
  totemStructures,
  elementsImage,
  leftRightOptions,
  aboveBelowOptions,
  insideOutiseOptions,
  containerOptions,
  leftRightBothOptions,
  skiesType,
  directionsOptions,
  operatorOptions,
  orderOptions,
  positionsStructures,
  exhibitorStructures,
  unitsOptions,
  errorMessages,
};
