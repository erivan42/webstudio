const questionTemplate = {
  exhibitor: {
    config: {
      name: '',
      template: 'exhibitor',
      frameStyle: 'EGYPT',
      frameNumber: 1,
      activityNumber: null,
      structure: 'sum',
    },
    params: {
      objectList: ['units', 'tens', 'hundreds', 'thousands'],
      quantityRange: [2, 4],
      charList: [
        'cat',
        'genie',
        'explorer',
        'mummy',
        'cleopatra',
        'odalisque',
        'camel',
      ],
      quantity: 7,
      squaresQuantity: [3, 6],
      displayPosition: ['left', 'right'],
      ask: [
        'Indique quais personagens que juntos tem a quantidade apresentada.',
      ],
    },
  },
  quiz: {
    config: {
      name: '',
      template: 'quiz',
      frameStyle: 'EGYPT',
      frameNumber: 1,
      activityNumber: null,
    },
    questions: [],
    structure: {
      statement: {
        type: 'TEXT',
        value: '',
      },
      answer: {
        type: 'TEXT',
        value: '',
      },
      alternatives: [],
    },
  },
  sequence: {
    config: {
      name: '',
      template: 'sequence',
      frameStyle: 'EGYPT',
      frameNumber: 1,
      activityNumber: null,
    },
    params: {
      range: [1, 3],
      squaresQuantity: 5,
      order: 'ascend',
      quantity: 7,
      ask: [
        'Quais números estão faltando?',
        'Quais números completam a sequência?',
        'Complete a sequência.',
      ],
    },
  },
  totem: {
    config: {
      name: '',
      template: 'totem',
      frameStyle: 'EGYPT',
      frameNumber: 1,
      activityNumber: null,
      structure: 'twoColumns',
    },
    params: {
      ask: ['Quem tem mais elementos?', 'Quem tem menos elementos?'],
      range: [8, 12],
      quantity: 3,
    },
  },
  positions: {
    config: {
      name: '',
      template: 'positions',
      frameStyle: 'EGYPT',
      frameNumber: 1,
      activityNumber: null,
      structure: 'order',
    },
    params: {
      quantity: 10,
      charList: ['explorer', 'cleopatra'],
      numCharsToShow: [1, 7],
      first: 'left',
      ask: [
        'Quem é o primeiro da fila?',
        'Quem é o segundo?',
        'Quem é o terceiro da fila?',
        'Quem é o quarto da fila?',
        'Quem é o quinto?',
        'Quem é o sexto da fila?',
        'Quem é o sétimo?',
        'Quem é o último?',
      ],
    },
  },
  counting: {
    config: {
      name: '',
      template: 'counting',
      frameStyle: 'EGYPT',
      frameNumber: 1,
      activityNumber: null,
      structure: 'quantity',
    },
    params: {
      quantity: 10,
      charList: [
        'cat',
        'genie',
        'explorer',
        'mummy',
        'cleopatra',
        'odalisque',
        'camel',
      ],
      listRange: [1, 10],
      ask: [
        'Quantas gatas tem?',
        'Conte os gênios.',
        'Quantos exploradores?',
        'Conte os camelos.',
        'Quantas múmias existem?',
        'Diga a quantidade de odaliscas.',
        'Quantas cleopatras você vê?',
        'Quantos personagens têm?',
      ],
    },
  },
  directions: {
    config: {
      name: '',
      template: 'directions',
      frameStyle: 'EGYPT',
      frameNumber: 4,
      activityNumber: null,
    },
    params: {
      directions: [
        'arrow_up',
        'arrow_down',
        'arrow_left',
        'arrow_right',
        'arrow_up_left',
        'arrow_up_right',
        'arrow_down_left',
        'arrow_down_right',
      ],
      quantity: 7,
      ask: ['Em que sentido o avião está voando?'],
    },
  },
  basketofballs: {
    config: {
      name: '',
      template: 'basketofballs',
      frameStyle: 'EGYPT',
      frameNumber: 1,
      activityNumber: null,
    },
    params: {
      quantity: 7,
      range: [8, 12],
      operator: '+',
      ask: [
        'Conte a quantidade de bolas na cesta?',
        'Qual o valor da operação?',
      ],
    },
  },
  equality: {
    config: {
      name: '',
      template: 'equality',
      frameStyle: 'EGYPT',
      frameNumber: 1,
      activityNumber: null,
    },
    params: {
      quantity: 3,
      subquestionsQuantity: 2,
      numbersRange: [1, 5],
      operator: '+',
      charList: ['genie', 'cleopatraBasket'],
      ask: [
        'Escolha o personagem da direita se o resultado da subtração estiver correto, escolha o personagem da esquerda se estiver incorreto.',
      ],
    },
  },
};

const templates = [
  { label: 'Não Cadastrado', color: '#FFF' },
  { value: 'quiz', label: 'Quiz', color: '#DC7633' },
  { value: 'sequence', label: 'Sequencias', color: '#45B39D' },
  { value: 'counting', label: 'Contagem', color: '#59D8E1' },
  { value: 'positions', label: 'Posições', color: '#C4C556' },
  { value: 'totem', label: 'Totem', color: '#7F8C8D' },
  { value: 'equality', label: 'Igualdade', color: '#9B59B6' },
  {
    value: 'basketofballs',
    label: 'Bolas na Cesta',
    color: '#D35400',
  },
  { value: 'exhibitor', label: 'Expositor', color: '#59B662' },
  { value: 'directions', label: 'Direções', color: '#2E86C1' },
];

const getNameTemplates = (name) => {
  const result = templates.find(
    (item) => item.value === name.toLowerCase(),
  );
  return result ? result : templates[0];
};

export { questionTemplate, templates, getNameTemplates };
