import {
  moreLessWordList,
  checkWordList,
} from './functions/checkWordList';
import useStyles from './useStyles';

export { moreLessWordList, checkWordList, useStyles };
