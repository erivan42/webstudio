import { moreWords, lessWords, helperText } from '../constants';

const moreLessWordList = (text) => {
  let more = false;
  let less = false;
  let result = {
    error: true,
    helperText: helperText.mustContain,
  };

  moreWords.forEach((word) => {
    if (text.search(word) !== -1) {
      more = true;
    }
  });

  lessWords.forEach((word) => {
    if (text.search(word) !== -1) {
      less = true;
    }
  });

  if (more && less) {
    result.helperText = helperText.mustHaveOne;
  } else if (more || less) {
    result = { error: false, helperText: '' };
  }

  if (text.length === 0) {
    result = { error: false, helperText: '' };
  }

  result.words = moreWords.concat(lessWords);

  return result;
};

const checkWordList = (txt, list) => {
  const text = txt.toLowerCase();
  let founded = false;
  let result = {
    error: true,
    helperText: helperText.mustContain,
  };

  list.forEach((word) => {
    if (text.search(word) !== -1) {
      founded = true;
    }
  });

  if (founded) {
    result = { error: false, helperText: '' };
  }

  if (text.length === 0) {
    result = { error: false, helperText: '' };
  }

  result.words = list;

  return result;
};

export { moreLessWordList, checkWordList };
