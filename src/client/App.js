import React from 'react';
import { Provider } from 'react-redux';
import store from './redux';
import Routes from './routes';
import { GlobalStyle } from './utils/useStyles';

const App = () => (
  <>
    <GlobalStyle />
    <Provider store={store}>
      <Routes />
    </Provider>
  </>
);

export default App;
