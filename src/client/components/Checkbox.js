import React, { useState } from 'react';
import {
  Checkbox,
  FormControlLabel,
  FormLabel,
  FormGroup,
} from '@material-ui/core/';
import styled from 'styled-components';
import { SimpleDialog } from './';
import { generalConstants } from '../utils/constants';

const CheckboxComp = (props) => {
  const { onChange, values, options, txt, field, width, min } = props;
  const [dialog, setDialog] = useState(false);

  const checkValue = (check) =>
    values.find((item) => item === check) ? true : false;

  const handleChange = (event) => {
    const newArray = [...values];
    const minimal = min ? min : 1;

    if (event.target.checked) {
      newArray.push(event.target.name);
      onChange(newArray, field);
    } else {
      const newArrayFiltered = values.filter(
        (value) => event.target.name !== value,
      );
      if (newArrayFiltered.length < minimal) {
        setDialog(true);
      } else {
        onChange(newArrayFiltered, field);
      }
    }
  };

  const handleCancel = () => {
    setDialog(false);
  };

  return (
    <Div width={width ? width : 300}>
      <SimpleDialog
        open={dialog}
        title={'Atenção'}
        onOk={handleCancel}
        text={`Deve existir ao menos ${min ? min : 'um'} campo${
          min ? 's' : ''
        } selecionado${min ? 's' : ''}.`}
      />

      <Label>
        <FormLabel component="legend">{txt}</FormLabel>
      </Label>
      <FormGroup row>
        {options.map((item, index) => (
          <FormControlLabel
            key={index}
            control={
              <Checkbox
                checked={checkValue(item.value)}
                onChange={handleChange}
                name={item.value}
                color="primary"
              />
            }
            label={item.label}
          />
        ))}
      </FormGroup>
    </Div>
  );
};

const Label = styled.div`
  margin: 5% 0 5% 2%;
`;

const Div = styled.div`
  width: ${(props) => props.width}px;
  margin: ${generalConstants.marginInnerCard};
  margin-left: 1%;
  display: flex;
  flex-direction: column;
`;

export default CheckboxComp;
