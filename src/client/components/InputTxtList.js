import React from 'react';
import { TextField, Tooltip } from '@material-ui/core/';
import { generalConstants } from '../utils/constants';
import styled from 'styled-components';
import { withStyles } from '@material-ui/core/styles';

const InputTxtList = (props) => {
  const {
    label,
    onChange,
    defaultValue,
    width,
    fullWidth,
    validator,
    field,
  } = props;
  let validators;

  const handleChange = (event) => {
    onChange(event.target.value, field);
  };

  const createTextTips = (words) => (
    <div>
      <p>Lista de palavras chaves a serem utilizadas:</p>
      <ul>
        {words.map((item, index) => (
          <li key={index}>{item}</li>
        ))}
      </ul>
    </div>
  );

  if (validator) {
    validators = validator(defaultValue);
  }

  return (
    <Div width={width ? width : 500}>
      <HtmlTooltip
        title={validator ? createTextTips(validators.words) : ''}
        placement="right"
      >
        <TextField
          label={label}
          value={defaultValue}
          variant="outlined"
          onChange={handleChange}
          fullWidth={fullWidth ? fullWidth : false}
          error={validators ? validators.error : false}
          helperText={validators ? validators.helperText : false}
        />
      </HtmlTooltip>
    </Div>
  );
};

const HtmlTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: '#f5f5f9',
    padding: '10%',
    color: 'rgba(0, 0, 0, 0.87)',
    maxWidth: 220,
    fontSize: theme.typography.pxToRem(14),
    border: '1px solid #dadde9',
    '&:after': {
      position: 'absolute',
      width: '35px',
      height: '35px',
      borderTop: '0px solid #dadde9',
      borderRight: '0px solid #dadde9',
      borderBottom: '2px solid #dadde9',
      borderLeft: '2px solid #dadde9',
      top: '40%',
      left: '-2%',
      content: '""',
      transform: 'rotate(45deg)',
      background: '#f5f5f9',
    },
  },
}))(Tooltip);

const Div = styled.div`
  width: ${(props) => props.width}px;
  margin: ${generalConstants.marginInnerCard};
  margin-left: 1%;
`;

export default InputTxtList;
