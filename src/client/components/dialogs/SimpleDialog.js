import React from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@material-ui/core/';

const SimpleDialog = (props) => {
  const { open, onCancel, onOk, title, text } = props;

  const handleCancel = () => {
    onCancel();
  };

  const handleOk = () => {
    onOk();
  };

  return (
    <Dialog open={open} onClose={onCancel ? onCancel : onOk}>
      <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {text}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          style={{ display: onCancel ? null : 'none' }}
          onClick={handleCancel}
          color="primary"
        >
          {'Cancelar'}
        </Button>
        <Button
          style={{ display: onOk ? null : 'none' }}
          onClick={handleOk}
          color="primary"
          autoFocus
        >
          {'Ok'}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default SimpleDialog;
