import React, { useState } from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
} from '@material-ui/core/';
import { AutocompleteCB } from '../';
import {
  questionTemplate,
  uniqueNumber,
  jsonCopy,
  templates,
} from '../../utils/constants';
import { Link } from 'react-router-dom';

const RemoveDialog = (props) => {
  const { open, handleClose } = props;

  const [template, setTemplate] = useState(null);

  const [templateQ, setTemplateQ] = useState(
    jsonCopy(questionTemplate),
  );

  const handleTemplate = (newValue) => {
    if (newValue) {
      templateQ[newValue].config.activityNumber = uniqueNumber();
      setTemplateQ(templateQ);
    }

    setTemplate(newValue);
  };

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        {'Adicionar Nova Questão'}
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {'Qual template você deseja utilizar?'}
        </DialogContentText>
        <AutocompleteCB
          txt={'Templates'}
          options={templates}
          value={template}
          onChange={handleTemplate}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={() => handleClose()} color="primary">
          Cancelar
        </Button>
        <Button
          color="primary"
          autoFocus
          disabled={!template ? true : false}
          component={Link}
          to={{
            pathname: `/${template ? template.toLowerCase() : '/'}`,
            state: {
              question: templateQ[template ? template : null],
              operation: 'add',
            },
          }}
        >
          Confirmar
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default RemoveDialog;
