import React from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
} from '@material-ui/core/';

const RemoveDialog = (props) => {
  const { open, handleClose, text } = props;

  return (
    <Dialog
      open={open}
      onClose={() => handleClose(0)}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">
        {`Remover Atividade "${text}"`}
      </DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {`Você tem certeza que deseja remover essa atividade "${text}"? `}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={() => handleClose(0)} color="primary">
          Cancelar
        </Button>
        <Button
          onClick={() => handleClose(1)}
          color="primary"
          autoFocus
        >
          Confirmar
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default RemoveDialog;
