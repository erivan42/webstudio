import React from 'react';
import { Chip } from '@material-ui/core/';
import { generalConstants } from '../utils/constants';
import styled from 'styled-components';

const Chips = (props) => {
  const { values } = props;

  return (
    <Div>
      {values.map((item, index) => (
        <Chip
          key={index}
          label={item}
          variant="outlined"
          color="primary"
        />
      ))}
    </Div>
  );
};

const Div = styled.div`
  width: 100%;
  margin: 0% 2%;
  display: flex;
  flex-flow: row wrap;
  justify-content: flex-start;
`;

export default Chips;
