import React from 'react';
import { useHistory } from 'react-router-dom';
import Structure from '../pages/login/Structure';
import styled from 'styled-components';

const NoMatchPage = () => {
  const history = useHistory();

  const onClick = () => {
    history.push({ pathname: '/' });
  };

  return (
    <Structure>
      <ContentDiv>
        <Text>404 - Nenhuma Página Encontrada</Text>
        <SubTxt onClick={() => onClick()}>
          Clique aqui para retornar
        </SubTxt>
      </ContentDiv>
    </Structure>
  );
};

const SubTxt = styled.p`
  font-weight: bold;
  color: #000;
  cursor: pointer;
`;

const Text = styled.h1`
  font-size: 3em;
  font-weight: bold;
  color: #707070;
`;

const ContentDiv = styled.div`
  width: 50%;
  height: 50%;
  margin: auto;
  padding: 25% 0;
`;

export default NoMatchPage;
