import React from 'react';
import { FormControl, InputLabel, Select } from '@material-ui/core/';
import styled from 'styled-components';
import { generalConstants } from '../utils/constants';

const ComboBox = (props) => {
  const { onChange, value, options, txt, width, field } = props;

  const handleChange = (event) => {
    onChange(event.target.value, field);
  };

  return (
    <Div>
      <FormControl style={{ minWidth: width ? width : 300 }}>
        <InputLabel htmlFor={`comboTitleTo-${txt}`}>{txt}</InputLabel>
        <Select
          native
          value={value}
          onChange={handleChange}
          inputProps={{
            name: field,
            id: `comboTitleTo-${txt}`,
          }}
        >
          {options.map((item, index) => (
            <option key={index} value={item.value}>
              {item.label}
            </option>
          ))}
        </Select>
      </FormControl>
    </Div>
  );
};

const Div = styled.div`
  width: ${(props) => props.width}px;
  margin: ${generalConstants.marginInnerCard};
  margin-left: 1%;
`;

export default ComboBox;
