import React, { useState, useEffect } from 'react';
import { useStyles, TabPanel, RemoveDialog } from './';
import { jsonCopy } from '../utils/constants';
import {
  AppBar,
  Tabs,
  Tab,
  Grid,
  IconButton,
  FormLabel,
  Tooltip,
} from '@material-ui/core/';
import {
  Add as AddIcon,
  Delete as DeleteIcon,
} from '@material-ui/icons';

const a11yProps = (index) => {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
};

const QuestionsTabs = (props) => {
  const {
    questions,
    children,
    onAddQuestion,
    structureQuestion,
  } = props;
  const [value, setValue] = useState(0);
  const [activities, setActivities] = useState(questions);
  const [removeDlg, setRemoveDlg] = useState(false);
  const [ind, setInd] = useState(null);
  const classes = useStyles();

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleCloseDlg = (agree) => {
    setRemoveDlg(!removeDlg);
    if (agree) {
      const newArray = activities.filter((_, index) => index !== ind);
      setActivities(newArray);
      setValue(0);
    }
  };

  const handleAdd = () =>
    setActivities([...activities, jsonCopy(structureQuestion)]);

  const handleRemove = (ind) => {
    setRemoveDlg(true);
    setInd(ind);
  };

  useEffect(() => {
    onAddQuestion(activities);
  }, [activities]);

  const AddButton = <Tab icon={<AddIcon />} onClick={handleAdd} />;

  const RemoveButton = (index, text) => (
    <>
      <RemoveDialog
        open={removeDlg}
        text={text}
        handleClose={handleCloseDlg}
      />
      <Grid
        container
        direction="row"
        justify="flex-end"
        alignItems="center"
      >
        <div onClick={() => handleRemove(index)}>
          <Tooltip title="Remover Questão" aria-label="remove">
            <IconButton aria-label="remove">
              <DeleteIcon fontSize="large" />
            </IconButton>
          </Tooltip>
        </div>
      </Grid>
    </>
  );

  return (
    <>
      <FormLabel component="label">Questões</FormLabel>

      <div className={classes.tabRoot}>
        <AppBar position="static" color="default">
          <Tabs
            value={value}
            onChange={handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="scrollable"
            scrollButtons="auto"
            aria-label="scrollable auto tabs example"
          >
            {questions.map((item, index) => (
              <Tab
                key={index}
                label={`Item ${index + 1}`}
                {...a11yProps(index)}
              />
            ))}
            {AddButton}
          </Tabs>
        </AppBar>
        {questions.map((item, index) => (
          <TabPanel key={index} value={value} index={index}>
            {RemoveButton(index, `Item ${index + 1}`)}

            {React.Children.map(children, (child, i) => {
              if (i === 0) {
                return React.cloneElement(child, {
                  question: item,
                  index: index,
                });
              } else {
                return child;
              }
            })}
          </TabPanel>
        ))}
      </div>
    </>
  );
};

export default QuestionsTabs;
