import React from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from '@material-ui/core/';
import styled from 'styled-components';
import { DeleteActBtn, EditActBtn, Tags } from '../components';
import { getNameTemplates } from '../utils/templates/questionTemplates';

const TableActivities = (props) => {
  const { activities } = props;

  return (
    <>
      <Title>Minhas Atividades</Title>
      <Table size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell>Nome</TableCell>
            <TableCell align="center">Tipo</TableCell>
            <TableCell align="right">Autor</TableCell>
            <TableCell align="right">Data</TableCell>
            <TableCell align="right">
              {'                     '}
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {activities.map((row, index) => (
            <TableRow key={index} hover>
              <TableCell component="th" scope="row">
                {row.config.name}
              </TableCell>
              <TableCell align="center">
                <Tags
                  label={getNameTemplates(row.config.template).label}
                  color={getNameTemplates(row.config.template).color}
                />
              </TableCell>
              <TableCell align="right">{row.config.author}</TableCell>
              <TableCell align="right">
                {new Date(row.config.timestamp).toLocaleString()}
              </TableCell>
              <TableCell>
                <EditActBtn question={row} index={index} />
                <DeleteActBtn activity={row} />
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </>
  );
};

const Title = styled.div`
  color: #707070;
  font-size: 1.8em;
  font-weight: bold;
  margin: 2% 0;
`;

export default TableActivities;
