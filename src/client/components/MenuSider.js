import React from 'react';
import { Drawer, Hidden, List } from '@material-ui/core/';
import { useStyles } from '../components/';
import { mainListItems } from '../components/listItem';
import styled from 'styled-components';
import { logo } from '../assets/';

const MenuSider = (props) => {
  const { mobileOpen, handleDrawerToggle } = props;
  const classes = useStyles();

  const Mobile = (
    <Hidden smUp implementation="css">
      <Drawer
        variant="temporary"
        anchor={'left'}
        open={mobileOpen}
        onClose={handleDrawerToggle}
        classes={{
          paper: classes.drawerPaper,
        }}
        ModalProps={{
          keepMounted: true, // Better open performance on mobile.
        }}
      >
        <DivLogo>
          <div className={classes.toolbarIcon}>
            <Img src={logo} />
          </div>
        </DivLogo>

        <List>{mainListItems}</List>
      </Drawer>
    </Hidden>
  );

  const Desktop = (
    <Hidden xsDown implementation="css">
      <Drawer
        classes={{
          paper: classes.drawerPaper,
        }}
        variant="permanent"
        open
      >
        <DivLogo>
          <DivImg>
            <Img src={logo} />
          </DivImg>
        </DivLogo>

        <List>{mainListItems}</List>
      </Drawer>
    </Hidden>
  );

  return (
    <nav className={classes.drawer} aria-label="mailbox folders">
      {Mobile}

      {Desktop}
    </nav>
  );
};

const DivLogo = styled.div`
  background-color: #0288d1;
  width: 100%;
`;

const DivImg = styled.div`
  margin: 2% auto;
  width: 80%;
`;

const Img = styled.img`
  width: 100%;
  height: 100%;
`;

export default MenuSider;
