import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { CssBaseline, Container, Card } from '@material-ui/core/';
import { Appbar, MenuSider, useStyles } from './';
import { connect } from 'react-redux';
import { checkUser } from '../actions/loginAction';

const Dashboard = (props) => {
  const classes = useStyles();
  const history = useHistory();
  const [mobileOpen, setMobileOpen] = useState(false);
  const [user, setUser] = useState({ name: '' });

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  useEffect(() => {
    props
      .checkUser()
      .then((resp) => {
        if (!resp.logged) {
          history.push({ pathname: '/signup' });
        }
        setUser(resp.user);
      })
      .catch((err) => console.log(err));
  }, []);

  return (
    <div className={classes.root}>
      <CssBaseline />
      <Appbar user={user} handleDrawerToggle={handleDrawerToggle} />
      <MenuSider
        mobileOpen={mobileOpen}
        handleDrawerToggle={handleDrawerToggle}
      />
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container maxWidth="lg" className={classes.container}>
          <Card className={classes.cardRoot}>{props.children}</Card>
        </Container>
      </main>
    </div>
  );
};

const mapStateToProps = (state) => ({
  user: state.loginReducer,
});

const mapDispatchToProps = { checkUser };

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Dashboard);
