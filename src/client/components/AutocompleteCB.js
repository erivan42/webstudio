import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import styled from 'styled-components';
import { generalConstants } from '../utils/constants';

const AutocompleteCB = (props) => {
  const { onChange, value, options, txt, width, field } = props;

  const currentValue = options.find((item) => item.value === value);

  const defaultProps = {
    options: options,
    getOptionLabel: (option) => (option ? option.label : ''),
  };

  return (
    <Div width={width ? width : 300}>
      <Autocomplete
        {...defaultProps}
        debug
        value={currentValue ? currentValue : ''}
        onChange={(event, newValue) =>
          onChange(newValue ? newValue.value : null, field)
        }
        renderInput={(params) => (
          <TextField {...params} label={txt} margin="normal" />
        )}
      />
    </Div>
  );
};

const Div = styled.div`
  width: ${(props) => props.width}px;
  margin: ${generalConstants.marginInnerCard};
  margin-left: 1%;
`;

export default AutocompleteCB;
