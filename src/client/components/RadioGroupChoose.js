import React from 'react';
import {
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
} from '@material-ui/core/';
import { generalConstants } from '../utils/constants';

const RadioGroupChoose = (props) => {
  const { options, choose, labelradio, onChange, field } = props;

  const handleChange = (event) => {
    onChange(event.target.value, field);
  };

  return (
    <div style={{ margin: generalConstants.marginBetweenElements }}>
      <FormControl component="fieldset">
        <FormLabel component="legend">{labelradio}</FormLabel>
        <RadioGroup
          aria-label="position"
          name="position"
          value={choose}
          onChange={handleChange}
          row
        >
          {options.map((item, index) => (
            <FormControlLabel
              key={index}
              value={item.value}
              control={<Radio color="primary" />}
              label={item.label}
              labelPlacement="start"
            />
          ))}
        </RadioGroup>
      </FormControl>
    </div>
  );
};

export default RadioGroupChoose;
