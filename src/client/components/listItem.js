import React from 'react';
import {
  ListItem,
  ListItemIcon,
  ListItemText,
} from '@material-ui/core/';
import { Link } from 'react-router-dom';
import { menus } from './';

export const mainListItems = (
  <div>
    {menus.map((item, index) => {
      if (!item.hide) {
        return (
          <ListItem
            key={index}
            button
            component={Link}
            to={item.path}
          >
            <ListItemIcon>{item.icon}</ListItemIcon>
            <ListItemText primary={item.name} />
          </ListItem>
        );
      }
    })}
  </div>
);
