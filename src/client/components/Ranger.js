import React from 'react';
import { Slider, FormLabel } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import styled from 'styled-components';
import { generalConstants } from '../utils/constants';

const Ranger = (props) => {
  const { onChange, value, txt, min, max, field } = props;

  const marks = [];

  for (let i = min ? min : 1; i <= max; i++) {
    marks.push({ value: i, label: i });
  }

  return (
    <Div>
      <Label>
        <FormLabel component="legend">{txt}</FormLabel>
      </Label>
      <CustomSlider
        value={value}
        min={min ? min : 1}
        max={max ? max : 10}
        marks={marks}
        valueLabelDisplay="on"
        onChange={(ev, number) => onChange(number, field)}
      />
    </Div>
  );
};

const Div = styled.div`
  width: 500px;
  margin: ${generalConstants.marginInnerCard};
`;

const Label = styled.div`
  margin: 5% 0 10% -4%;
`;

const BoxShadow =
  '0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.13),0 0 0 1px rgba(0,0,0,0.02)';

const CustomSlider = withStyles({
  root: {
    color: '#3880ff',
    height: 2,
    padding: '15px 0',
  },
  thumb: {
    height: 28,
    width: 28,
    backgroundColor: '#fff',
    boxShadow: BoxShadow,
    marginTop: -14,
    marginLeft: -14,
    '&:focus, &:hover, &$active': {
      boxShadow:
        '0 3px 1px rgba(0,0,0,0.1),0 4px 8px rgba(0,0,0,0.3),0 0 0 1px rgba(0,0,0,0.02)',
      // Reset on touch devices, it doesn't add specificity
      '@media (hover: none)': {
        boxShadow: BoxShadow,
      },
    },
  },
  active: {},
  valueLabel: {
    left: 'calc(-50% + 11px)',
    top: -22,
    '& *': {
      background: 'transparent',
      color: '#000',
    },
  },
  track: {
    height: 2,
  },
  rail: {
    height: 2,
    opacity: 0.5,
    backgroundColor: '#bfbfbf',
  },
  mark: {
    backgroundColor: '#bfbfbf',
    height: 8,
    width: 1,
    marginTop: -3,
  },
  markActive: {
    opacity: 1,
    backgroundColor: 'currentColor',
  },
})(Slider);

export default Ranger;
