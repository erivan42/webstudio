import React from 'react';
import {
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
} from '@material-ui/core/';
import styled from 'styled-components';
import { DeleteActBtn, EditActBtn, Tags } from '../components';
import { getNameTemplates } from '../utils/templates/questionTemplates';

const TableVersions = (props) => {
  const { activities } = props;

  return (
    <>
      <Title>Versões da Aplicação</Title>
      <Table size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell>Versão</TableCell>
            <TableCell align="right">
              Quantidade de Atividades
            </TableCell>
            <TableCell align="right">Data de Criação</TableCell>
            <TableCell align="right">
              {'                     '}
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody></TableBody>
      </Table>
    </>
  );
};

const Title = styled.div`
  color: #707070;
  font-size: 1.8em;
  font-weight: bold;
  margin: 2% 0;
`;

export default TableVersions;
