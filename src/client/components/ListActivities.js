import React from 'react';
import {
  List,
  ListItem,
  Avatar,
  ListItemText,
  ListItemSecondaryAction,
  ListItemAvatar,
  IconButton,
} from '@material-ui/core/';
import {
  Edit as EditIcon,
  Folder as FolderIcon,
} from '@material-ui/icons/';
import { DeleteActBtn } from '../components';

import { Link } from 'react-router-dom';

const ListActivities = (props) => {
  const { activities } = props;

  return (
    <List>
      {activities.map((item, index) => (
        <ListItem
          key={index}
          component={Link}
          to={{
            pathname: `/${item.config.template.toLowerCase()}`,
            state: {
              question: item,
              operation: 'edit',
            },
          }}
        >
          <ListItemAvatar>
            <Avatar>
              <FolderIcon />
            </Avatar>
          </ListItemAvatar>
          <ListItemText
            primary={item.config.name}
            secondary={`Tipo do template: ${item.config.template}`}
          />
          <ListItemSecondaryAction>
            <IconButton
              edge="end"
              aria-label="edit"
              component={Link}
              to={{
                pathname: `/${item.config.template.toLowerCase()}`,
                state: {
                  question: item,
                  operation: 'edit',
                },
              }}
            >
              <EditIcon />
            </IconButton>
            <DeleteActBtn question={item} activities={activities} />
          </ListItemSecondaryAction>
        </ListItem>
      ))}
    </List>
  );
};

export default ListActivities;
