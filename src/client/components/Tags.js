import React from 'react';
import styled from 'styled-components';

const Tags = (props) => {
  const { label, color } = props;

  return <Tag color={color}>{label}</Tag>;
};

const Tag = styled.div`
  margin: 0 auto;
  background-color: ${(props) => props.color};
  color: #fff;
  padding: 0.5em 0.5em;
  width: 80%;
  border-radius: 30px;
  :hover {
    transform: scale(1.1);
  }
`;

export default Tags;
