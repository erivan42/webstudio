import React from 'react';
import { TextValidator } from 'react-material-ui-form-validator';
import { generalConstants } from '../utils/constants';
import styled from 'styled-components';

const InputTxt = (props) => {
  const {
    label,
    onChange,
    defaultValue,
    width,
    fullWidth,
    field,
    validators,
    errorMessages,
  } = props;

  const handleChange = (event) => {
    onChange(event.target.value, field);
  };

  return (
    <Div width={width ? width : 500}>
      <TextValidator
        label={label}
        onChange={handleChange}
        name={label}
        value={defaultValue}
        fullWidth={fullWidth ? fullWidth : false}
        validators={validators}
        errorMessages={errorMessages}
      />
    </Div>
  );
};

const Div = styled.div`
  width: ${(props) => props.width}px;
  margin: ${generalConstants.marginInnerCard};
  margin-left: 1%;
`;

export default InputTxt;
