import React, { useState } from 'react';
import {
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  FormLabel,
  Avatar,
  ListItemAvatar,
} from '@material-ui/core';
import { Delete, ShortText } from '@material-ui/icons';
import styled from 'styled-components';
import { InputTxtList, CircularBtn, SimpleDialog } from './';

const Asks = (props) => {
  const { onChange, array, txt, field, validator } = props;
  const [value, setValue] = useState('');
  const [dialog, setDialog] = useState(false);

  const handleAdd = () => {
    if (validator) {
      const result = validator(value);
      if (result.error) {
        return;
      }
    }

    if (value.length !== 0) {
      const newArray = [...array];
      newArray.push(value);
      onChange(newArray, field);
      setValue('');
    }
  };

  const handleDelete = (ind) => {
    const newArray = array.filter((_, index) => index !== ind);
    if (newArray.length !== 0) {
      onChange(newArray, field);
    } else {
      setDialog(true);
    }
  };

  const handleValue = (value) => {
    setValue(value);
  };

  const handleCancel = () => {
    setDialog(false);
  };

  return (
    <Div>
      <SimpleDialog
        open={dialog}
        title={'Atenção'}
        onOk={handleCancel}
        text={'Deve haver pelo menos um elemento na lista'}
      />

      <Label>
        <FormLabel component="legend">{txt}</FormLabel>
      </Label>

      <List>
        <ListItem>
          <InputTxtList
            label={'Titulo da Atividade'}
            defaultValue={value}
            onChange={handleValue}
            validator={validator}
            fullWidth
          />
          <ListItemSecondaryAction>
            <CircularBtn onClick={handleAdd} />
          </ListItemSecondaryAction>
        </ListItem>
      </List>

      <List>
        {array.map((item, index) => (
          <ListItem key={index}>
            <ListItemAvatar>
              <Avatar>
                <ShortText />
              </Avatar>
            </ListItemAvatar>
            <ListItemText primary={item} />
            <ListItemSecondaryAction>
              <IconButton
                edge="end"
                aria-label="delete"
                onClick={() => handleDelete(index)}
              >
                <Delete />
              </IconButton>
            </ListItemSecondaryAction>
          </ListItem>
        ))}
      </List>
    </Div>
  );
};

const Div = styled.div`
  width: 80%;
`;

const Label = styled.div`
  margin: 5% 0 0% 2%;
`;

export default Asks;
