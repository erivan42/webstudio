import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Grid, Typography } from '@material-ui/core/';
import { ReturnBtn, Button, SimpleDialog } from './';
import styled from 'styled-components';
import { generalConstants, urlApp } from '../utils/constants';
import { useHistory, useLocation } from 'react-router-dom';
import {
  getActivities,
  addActivity,
  updateActivity,
} from '../actions/activitiesAction';
import { ValidatorForm } from 'react-material-ui-form-validator';

const ActivityView = (props) => {
  const { children, title, activity, validation } = props;
  const [dialog, setDialog] = useState(false);
  const [errors, setErrors] = useState([]);
  const history = useHistory();
  const location = useLocation();
  const [view, setView] = useState(false);
  const [viewNewAct, setViewNewAct] = useState(false);
  const [edited, setEdited] = useState(false);
  const [editedIndex, setEditedIndex] = useState(0);

  const buttonClickSave = () => {
    const validated = validation ? validation() : true;

    if (validated) {
      handleSave();
    }
  };
  const buttonClickView = (event) => {
    const validated = validation ? validation() : true;
    if (validated) {
      handleView();
    }
  };

  const handleSave = () => {
    if (location.state.operation === 'add') {
      props.addActivity(activity);
    }
    if (location.state.operation === 'edit') {
      props.updateActivity(activity, location.state.index);
    }
    history.push({ pathname: '/dashboard' });
  };

  const handleView = () => {
    if (location.state.operation === 'add') {
      setView(true);
      props.addActivity(activity).then((okAdd) => {
        props.getActivities().then((act) => {
          location.state.operation = 'edit';
          setViewNewAct(true);
          const activityNumber = act.payload.activities.length - 1;

          window.open(
            `${urlApp}${location.pathname}/${activityNumber}`,
          );
        });
      });
    } else if (location.state.operation === 'edit') {
      if (!viewNewAct) {
        const activityNumber = location.state.index;
        props.updateActivity(activity, activityNumber);
        setEdited(true);
        setEditedIndex(activityNumber);
        window.open(
          `${urlApp}${location.pathname}/${activityNumber}`,
        );
      } else {
        props.getActivities().then((act) => {
          setEdited(false);
          const activityNumber = act.payload.activities.length - 1;
          props
            .updateActivity(activity, activityNumber)
            .then((okEdit) => {
              window.open(
                `${urlApp}${location.pathname}/${activityNumber}`,
              );
            });
        });
      }
    }
  };

  const handleCancel = () => {
    setDialog(false);
  };

  useEffect(() => {
    if (errors.length > 0) {
      setDialog(true);
    }
  }, [errors]);

  return (
    <ValidatorForm
      onSubmit={buttonClickSave}
      onError={(errorsForm) => setErrors(errorsForm)}
    >
      <SimpleDialog
        open={dialog}
        title={'Atenção'}
        onOk={handleCancel}
        text={
          'Existem campos não válidos. Por favor, preencha corretamente todos os campos'
        }
      />
      <Div>
        <Grid
          container
          direction="column"
          justify="space-around"
          alignItems="flex-start"
        >
          <Typography variant="h5" component="h2">
            <Title>{title}</Title>
          </Typography>

          {children}

          <Grid
            container
            direction="row"
            justify="flex-end"
            alignItems="flex-start"
            spacing={2}
          >
            <Grid item>
              <ReturnBtn
                edited={edited}
                editedIndex={editedIndex}
                activity={activity}
                view={view}
              />
            </Grid>
            <Grid item>
              <Button type="submit" text={'SALVAR'} />
            </Grid>
            <Grid item>
              <Button
                onClick={buttonClickView}
                type="button"
                color="linear-gradient(180deg, #00983B 0%, #00B70E 100%)"
                hover="#00D552"
                text={'SALVAR E VISUALIZAR'}
              />
            </Grid>
          </Grid>
        </Grid>
      </Div>
    </ValidatorForm>
  );
};

const Div = styled.div`
  margin: ${generalConstants.marginInnerCard};
`;

const Title = styled.p`
  font-weight: bolder;
  color: #707070;
`;

const mapDispatchToProps = {
  getActivities,
  addActivity,
  updateActivity,
};

const mapStateToProps = (state) => ({
  activities: state.activitiesReducer,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ActivityView);
