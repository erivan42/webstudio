import React from 'react';
import {
  Toolbar,
  AppBar,
  IconButton,
  Avatar,
} from '@material-ui/core/';
import { Menu as MenuIcon, AccountCircle } from '@material-ui/icons/';
import { useStyles } from '../components/';
import styled from 'styled-components';

const Appbar = (props) => {
  const { handleDrawerToggle, user } = props;
  const classes = useStyles();

  return (
    <AppBar position="fixed" className={classes.appBar}>
      <Toolbar className={classes.toolbar}>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          edge="start"
          onClick={handleDrawerToggle}
          className={classes.menuButton}
        >
          <MenuIcon />
        </IconButton>

        <Div>
          <Name>{user.name}</Name>
          <Avatar src={null}>
            <AccountCircle />
          </Avatar>
        </Div>
      </Toolbar>
    </AppBar>
  );
};

const Name = styled.p`
  margin-right: 10px;
  font-size: 1.1em;
  font-weight: bold;
  color: #707070;
`;

const Div = styled.div`
  position: absolute;
  float: right;
  right: 2px;
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-right: 10px;
`;

export default Appbar;
