import React from 'react';
import { IconButton, Tooltip } from '@material-ui/core/';
import { Add as AddIcon } from '@material-ui/icons';

const CircularBtn = (props) => {
  return (
    <Tooltip title="Adicionar" aria-label="add">
      <IconButton aria-label="add" onClick={props.onClick}>
        <AddIcon fontSize="large" style={props.style} />
      </IconButton>
    </Tooltip>
  );
};

export default CircularBtn;
