import React, { useState } from 'react';
import { connect } from 'react-redux';
import {
  Add as AddIcon,
  AddCircle,
  OutdoorGrill as GenerateIcon,
  GetApp as DownloadIcon,
} from '@material-ui/icons';
import { SpeedDial, SpeedDialAction } from '@material-ui/lab';
import { generateApplication } from '../../actions/activitiesAction';
import { useStyles } from '../';

const FabDashboard = (props) => {
  const { onAdd, generateApplication } = props;
  const classes = useStyles();
  const [open, setOpen] = useState(false);

  const onGenerate = () => {
    generateApplication();
  };

  const onDownload = () => {
    const win = window.open('/api/download', '_blank');
    win.focus();
  };

  const dashMenus = [
    { icon: <AddCircle />, name: 'Nova Atividade', onClick: onAdd },
    {
      icon: <GenerateIcon />,
      name: 'Gerar Nova Versão',
      onClick: onGenerate,
    },
    {
      icon: <DownloadIcon />,
      name: 'Baixar Aplicação',
      onClick: onDownload,
    },
  ];

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  return (
    <SpeedDial
      ariaLabel="Dashboard Options"
      className={classes.fab}
      icon={<AddIcon />}
      onClose={handleClose}
      onOpen={handleOpen}
      open={open}
      direction={'up'}
    >
      {dashMenus.map((item) => (
        <SpeedDialAction
          key={item.name}
          icon={item.icon}
          tooltipTitle={item.name}
          onClick={item.onClick}
        />
      ))}
    </SpeedDial>
  );
};

const mapDispatchToProps = { generateApplication };

export default connect(null, mapDispatchToProps)(FabDashboard);
