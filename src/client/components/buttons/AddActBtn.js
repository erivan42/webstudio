import React, { useState } from 'react';
import { TemplateDialog, Fab } from '../';

const AddActBtn = () => {
  const [open, setOpen] = useState(false);

  const handleOpenDlg = () => {
    setOpen(!open);
  };

  const handleCloseDlg = () => {
    setOpen(!open);
  };

  return (
    <>
      <TemplateDialog open={open} handleClose={handleCloseDlg} />
      <Fab onClick={handleOpenDlg} />
    </>
  );
};

export default AddActBtn;
