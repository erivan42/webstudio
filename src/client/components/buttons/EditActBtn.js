import React from 'react';
import { IconButton } from '@material-ui/core/';
import { Link } from 'react-router-dom';
import { Edit as EditIcon } from '@material-ui/icons/';

const EditActBtn = (props) => (
  <>
    <IconButton
      edge="end"
      aria-label="edit"
      component={Link}
      to={{
        pathname: `/${props.question.config.template.toLowerCase()}`,
        state: {
          question: props.question,
          operation: 'edit',
          index: props.index,
        },
      }}
    >
      <EditIcon />
    </IconButton>
  </>
);

export default EditActBtn;
