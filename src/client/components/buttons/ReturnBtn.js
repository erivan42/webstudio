import React from 'react';
import { Link } from 'react-router-dom';
import { ButtonWhite } from '../';
import {
  updateActivity,
  removeActivity,
} from '../../actions/activitiesAction';
import { connect } from 'react-redux';

const handleReturn = (
  event,
  editedIndex,
  edited,
  previousActState,
  activity,
  view,
  removeActivity,
) => {
  if (edited) {
    updateActivity(previousActState, editedIndex);
  } else if (activity && view) {
    removeActivity(activity.config.activityNumber);
  }
};

const ReturnBtn = (props) => {
  const {
    edited,
    editedIndex,
    previousActState,
    activity,
    view,
    removeActivity,
  } = props;
  return (
    <Link
      onClick={(event) =>
        handleReturn(
          event,
          edited,
          editedIndex,
          previousActState,
          activity,
          view,
          removeActivity,
        )
      }
      to={{
        pathname: '/',
        state: true,
      }}
    >
      <ButtonWhite grey text={'RETORNAR'} />
    </Link>
  );
};

const mapDispatchToProps = {
  updateActivity,
  removeActivity,
};

export default connect(null, mapDispatchToProps)(ReturnBtn);
