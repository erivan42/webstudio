import React, { useState } from 'react';
import { connect } from 'react-redux';
import { IconButton } from '@material-ui/core/';
import { Delete as DeleteIcon } from '@material-ui/icons/';
import { removeActivity } from '../../actions/activitiesAction';
import { RemoveActDialog } from '../';

const DeleteActBtn = (props) => {
  const { activity } = props;
  const [removeDlg, setRemoveDlg] = useState(false);

  const onClickBtn = () => {
    setRemoveDlg(true);
  };

  const handleCloseDlg = (agree) => {
    setRemoveDlg(!removeDlg);
    if (agree) {
      props.removeActivity(activity.config.activityNumber);
    }
  };

  return (
    <>
      <RemoveActDialog
        open={removeDlg}
        text={activity.config.name}
        handleClose={handleCloseDlg}
      />
      <IconButton edge="end" aria-label="delete" onClick={onClickBtn}>
        <DeleteIcon />
      </IconButton>
    </>
  );
};

const mapDispatchToProps = { removeActivity };

export default connect(null, mapDispatchToProps)(DeleteActBtn);
