import React from 'react';
import { CircularBtn } from '../';
import styled from 'styled-components';

const Fab = (props) => (
  <FabDiv>
    <CircularBtn onClick={props.onClick} style={{ color: '#fff' }} />
  </FabDiv>
);

const FabDiv = styled.div`
  background: transparent
    linear-gradient(180deg, #0288d1 0%, #0699ea 100%) 0% 0% no-repeat
    padding-box;
  box-shadow: 0px 6px 16px #0000003d;
  border-radius: 100%;
  position: absolute;
  cursor: pointer;
  bottom: 2%;
  right: 2%;
  :hover {
    background: #3bccf6 0% 0% no-repeat padding-box;
  }
`;

export default Fab;
