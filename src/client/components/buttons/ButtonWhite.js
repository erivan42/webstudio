import React from 'react';
import styled from 'styled-components';

const ButtonWhite = (props) => {
  const { text, fullWidth, grey, onClick } = props;

  return (
    <Btn onClick={onClick} grey={grey} fullWidth={fullWidth}>
      {text}
    </Btn>
  );
};

const Btn = styled.button`
  background-color: white;
  color: ${(props) => (props.grey ? '#0000003D' : '#0288d1')};
  font-weight: bold;
  font-size: 1em;
  padding: 1em 3.5em;
  cursor: pointer;
  text-decoration: none;
  border: 1px solid #fff;
  border-radius: 20px;
  margin: 2% 1%;
  ${(props) => (props.fullWidth ? 'width: 100%;' : null)};
  :hover {
    border: 1px solid #f9f9f9;
    border-radius: 20px;
    background-color: #f9f9f9;
    text-decoration: none;
    outline: none;
  }
  :active {
    position: relative;
    top: 2px;
  }
`;

export default ButtonWhite;
