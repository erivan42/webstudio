import React from 'react';
import styled from 'styled-components';

const Button = (props) => {
  const { text } = props;

  return <Btn {...props}>{text}</Btn>;
};

const Btn = styled.button`
  background: ${(props) =>
      props.color
        ? props.color
        : 'transparent linear-gradient(180deg, #0288d1 0%, #0699ea 100%)'}
    0% 0% no-repeat padding-box;
  box-shadow: 0px 6px 16px #0000003d;
  border-radius: 15px;
  margin: 2% 1%;
  color: #fff;
  font-weight: bold;
  font-size: 1em;
  padding: 1em 3.5em;
  border: 0px solid #000;
  cursor: pointer;
  text-decoration: none;
  outline: none;
  ${(props) => (props.fullWidth ? 'width: 100%;' : null)};
  :hover {
    color: #fff;
    background: ${(props) => (props.hover ? props.hover : '#3bccf6')}
      0% 0% no-repeat padding-box;
  }
  :active {
    position: relative;
    top: 2px;
  }
`;

export default Button;
