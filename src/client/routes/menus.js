import React from 'react';
import { ExitToApp } from '@material-ui/icons/';
import { dashboardIcon, versionsIcon } from '../assets/icons';
import Home from '../pages/Home';
import DashboardPage from '../pages/dashboard';
import Versions from '../pages/versions';
import Quiz from '../pages/quiz';
import Sequences from '../pages/sequences';
import Totem from '../pages/totem';
import Counting from '../pages/counting';
import Equality from '../pages/equality';
import Directions from '../pages/directions';
import Exhibitor from '../pages/exhibitor';
import Positions from '../pages/positions';
import Basketofballs from '../pages/basketofballs';
import { Signin, Signup, Logout } from '../pages/login';

const menus = [
  {
    main: true,
    hide: true,
    path: '/',
    name: 'Home',
    component: <Home />,
  },
  {
    path: '/dashboard',
    icon: <img src={dashboardIcon} alt="dashboard" />,
    name: 'Dashboard',
    component: <DashboardPage />,
  },
  {
    page: false,
    hide: false,
    path: '/versions',
    icon: <img src={versionsIcon} alt="versions" />,
    name: 'Versões',
    component: <Versions />,
  },
  {
    page: true,
    hide: true,
    path: '/signin',
    name: 'Signin',
    component: <Signin />,
  },
  {
    page: true,
    hide: true,
    path: '/signup',
    name: 'Signup',
    component: <Signup />,
  },
  {
    hide: true,
    path: '/quiz',
    name: 'Quiz',
    component: <Quiz />,
  },
  {
    hide: true,
    path: '/sequence',
    name: 'Sequence',
    component: <Sequences />,
  },
  {
    hide: true,
    path: '/totem',
    name: 'Totem',
    component: <Totem />,
  },
  {
    hide: true,
    path: '/counting',
    name: 'Counting',
    component: <Counting />,
  },
  {
    hide: true,
    path: '/equality',
    name: 'Equality',
    component: <Equality />,
  },
  {
    hide: true,
    path: '/directions',
    name: 'Directions',
    component: <Directions />,
  },
  {
    hide: true,
    path: '/exhibitor',
    name: 'Exhibitor',
    component: <Exhibitor />,
  },
  {
    hide: true,
    path: '/positions',
    name: 'Positions',
    component: <Positions />,
  },
  {
    hide: true,
    path: '/basketofballs',
    name: 'Basketofballs',
    component: <Basketofballs />,
  },
  {
    hide: false,
    path: '/logout',
    icon: <ExitToApp style={{ color: 'white' }} />,
    name: 'Logout',
    component: <Logout />,
  },
];

export default menus;
