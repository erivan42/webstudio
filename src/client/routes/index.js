import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';
import { menus, Dashboard, NoMatchPage } from '../components/';

const main = (item, index) => (
  <Route exact key={index} path="/">
    {item.component}
  </Route>
);

const page = (item, index) => (
  <Route key={index} path={item.path}>
    {item.component}
  </Route>
);

const other = (item, index) => (
  <Route key={index} path={item.path}>
    <Dashboard>{item.component}</Dashboard>
  </Route>
);

const Routes = () => (
  <Router>
    <Switch>
      {menus.map((item, index) =>
        item.main
          ? main(item, index)
          : item.page
          ? page(item, index)
          : other(item, index),
      )}
      <Route>
        <NoMatchPage />
      </Route>
    </Switch>
  </Router>
);

export default Routes;
