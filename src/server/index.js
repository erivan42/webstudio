const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const fs = require('fs');
const { generator } = require('./generator.js');
const login = require('./login.js');
const { pathServer, pathLocal } = require('./constants');

let fileJson;

const pathToFile =
  typeof process.env.NODE_ENV !== 'undefined'
    ? pathServer.activites
    : pathLocal.activites;
const dist =
  typeof process.env.NODE_ENV !== 'undefined'
    ? pathServer.dist
    : pathLocal.dist;

const port = 3001;

app.use(bodyParser.json());

app.use(express.static(dist));

app.get('/api/getActivities', (req, res) => {
  fs.readFile(pathToFile, (err, data) => {
    if (err) {
      throw err;
    }
    fileJson = JSON.parse(data);
    res.send(fileJson);
  });
});

app.post('/api/addActivity', (req, res) => {
  const data = req.body;
  fileJson.activities.push(data.activity);
  fs.writeFile(pathToFile, `${JSON.stringify(fileJson)}`, () => {
    res.send({ res: 'activities updated in json file' });
  });
});

app.post('/api/updateActivity', (req, res) => {
  const data = req.body;
  fileJson.activities[data.index] = data.activity;
  fs.writeFile(pathToFile, `${JSON.stringify(fileJson)}`, () => {
    res.send({ res: 'activities updated in json file' });
  });
});

app.post('/api/removeActivity', (req, res) => {
  const data = req.body;
  fileJson.activities = fileJson.activities.filter(
    (item) => item.config.activityNumber !== data.activityNumber,
  );

  fs.writeFile(pathToFile, `${JSON.stringify(fileJson)}`, () => {
    res.send({ res: 'activities updated in json file' });
  });
});

app.get('/api/generateApplication', (req, res) => {
  generator();
  res.send({ res: true });
});

app.get('/api/download', function(req, res) {
  const file = `${__dirname}/eblocksApp.zip`;
  res.download(file); // Set disposition and send it.
});

login(app, express);

app.listen(
  process.env.NODEJS_PORT || port,
  process.env.NODEJS_ADDR,
  () =>
    console.log(
      `Listening on port ${process.env.NODEJS_PORT || port}!`,
    ),
);
