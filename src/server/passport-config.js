const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');

const initalize = (passport, getUsersByEmail, getUsersById) => {
  const authenticateUser = async (email, password, done) => {
    const user = getUsersByEmail(email);
    if (user == null) {
      return done(null, false, {
        message: 'noEmail',
      });
    }

    try {
      if (await bcrypt.compare(password, user.password)) {
        return done(null, user);
      } else {
        return done(null, false, { message: 'passwordIncorrect' });
      }
    } catch (e) {
      return done(e);
    }
  };

  passport.use(
    new LocalStrategy({ usernameField: 'email' }, authenticateUser),
  );

  //Controle session users
  passport.serializeUser((user, done) => done(null, user.id));
  passport.deserializeUser((id, done) =>
    done(null, getUsersById(id)),
  );
};

module.exports = initalize;
