if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config();
}

const session = require('express-session');
const flash = require('express-flash');
const methodOverride = require('method-override');
const bcrypt = require('bcrypt');
const passport = require('passport');

const initializePassport = require('./passport-config');
initializePassport(
  passport,
  (email) => users.find((user) => user.email === email),
  (id) => users.find((user) => user.id === id),
);

const users = [];

const login = (app, express) => {
  app.use(express.urlencoded({ extended: false }));
  app.use(methodOverride('_method'));
  app.use(flash());
  app.use(
    session({
      secret: 'process.env.SESSION_SECRET',
      resave: false,
      saveUninitialized: false,
    }),
  );

  app.use(passport.initialize());
  app.use(passport.session());

  app.get('/api/checkuser', (req, res) => {
    res.send({
      logged: req.isAuthenticated() ? true : false,
      user: req.user ? req.user : {},
    });
  });

  app.post('/api/login', (req, res, next) => {
    passport.authenticate('local', (err, user, info) => {
      if (info) {
        return res.send({ info: info, logged: false });
      } else {
        req.logIn(user, (err) => {
          if (err) {
            return next(err);
          }
          return res.send({ user: user });
        });
      }
    })(req, res, next);
  });

  app.post('/api/logout', (req, res) => {
    req.logOut();
    res.send(true);
  });

  app.post('/register', checkNotAuthenticated, async (req, res) => {
    try {
      const hashedPassword = await bcrypt.hash(req.body.password, 10);
      users.push({
        id: Date.now().toString(),
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword,
      });
      res.send('/login');
    } catch {
      res.send('/register');
    }
  });
};

const checkNotAuthenticated = (req, res, next) => {
  if (!req.isAuthenticated()) {
    return res.send({ logged: false, user: {} });
  } else {
    return next();
  }
};

const userAdmin = async () => {
  const hashedPassword = await bcrypt.hash('123', 10);

  users.push({
    id: Date.now().toString(),
    name: 'admin',
    email: 'admin@admin',
    password: hashedPassword,
  });

  users.push({
    id: Date.now().toString() + 'a',
    name: 'admin2',
    email: 'admin2@admin2',
    password: hashedPassword,
  });
};

if (users.length === 0) {
  userAdmin();
}

module.exports = login;
