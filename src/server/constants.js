const path = require('path');

const pathLocal = {
  activites:
    '../softex/src/stages/utils/metaActivities/metaActivities.json',
  dist: 'dist',
};

const pathServer = {
  activites: path.join(
    __dirname,
    '../../../../../appwebstudio.educacional.com.br/app/electron/src/stages/utils/metaActivities/metaActivities.json',
  ),
  dist: path.join(__dirname, '../../dist'),
};

const urlApp =
  typeof process.env.NODE_ENV !== 'undefined'
    ? 'http://dev-appwebstudio.educacional.com.br/#'
    : 'http://localhost:3000/#';

module.exports = {
  pathServer,
  pathLocal,
  urlApp,
};
