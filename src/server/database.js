const knex = require('knex')({
  client: 'mysql',
  connection: {
    host: '127.0.0.1',
    port: '3306',
    user: 'root',
    password: 'monteiro',
    database: 'new_schema',
  },
});

const dataBase = async () => {
  const res = await knex.select().table('new_table');
};

module.exports = dataBase;
