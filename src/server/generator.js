const { zip } = require('zip-a-folder');
const cmd = require('node-cmd');

const generator = () => {
  cmd.get(
    'cd ../softex && npm run electron-pack',
    (err, data, stderr) => {
      if (!err) {
        console.log('Application packed in Electron :\n\n', data);
        buildGenerator();
      } else {
        console.log('error', err);
      }
    },
  );
};

const buildGenerator = () => {
  cmd.get(
    'cd ../softex && npm run electron-build',
    (err, data, stderr) => {
      if (!err) {
        console.log('Application built in Electron :\n\n', data);
        createZipFolder();
      } else {
        console.log('error', err);
      }
    },
  );
};

const createZipFolder = () => {
  console.log('File Zipped');
  zip(
    `${__dirname}/../../../softex/dist`,
    `${__dirname}/eblocksApp.zip`,
  );
};

module.exports = {
  generator,
};
